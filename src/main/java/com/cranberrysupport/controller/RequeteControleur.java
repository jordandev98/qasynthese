package com.cranberrysupport.controller;

import com.cranberrysupport.model.*;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

public class RequeteControleur {
    public void addRequeteNouvelleRequete(Requete requete) throws IOException, SQLException {
        BanqueRequetes.getInstance().addNouvelleRequete(requete);
    }

    public List<Requete> getListeRequete(RequeteStatut statut) throws IOException, SQLException {
        return BanqueRequetes.getInstance().getListRequetes(statut);
    }

    public void assignerRequete(Technicien technicien, Requete requeteSelectionee) throws IOException, SQLException {
        BanqueRequetes.getInstance().assignerRequete(technicien, requeteSelectionee);
    }

    public Requete getDerniereRequete() throws IOException, SQLException {
        return BanqueRequetes.getInstance().getDerniereRequete();
    }

    public void saveRequetes() throws IOException, SQLException {
        BanqueRequetes.getInstance().saveRequetes();
    }

    public List<Requete> getRequetesByClient(Client client) throws IOException, SQLException {
        return BanqueRequetes.getInstance().getRequetesByClient(client);
    }


}
