package com.cranberrysupport.util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class JdbcUtil {

    private String JDBC_DRIVER = "org.h2.Driver";
    private static  String DB_URL = "jdbc:h2:~/test";

    //  Database credentials
    private static String USER = "sa";
    private static String PASS = "sa";

    public static Connection getConnection() throws SQLException {
        return  DriverManager.getConnection(DB_URL, USER, PASS);
    }

}
