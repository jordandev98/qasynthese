package com.cranberrysupport.util;

import com.cranberrysupport.model.Client;
import com.cranberrysupport.model.Technicien;
import com.cranberrysupport.model.UserRole;
import com.cranberrysupport.model.Utilisateur;

import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class UtilisateurDao {

	public UtilisateurDao() throws SQLException {
	}

	public void creerTableUtilisateur() throws SQLException {
		Connection connection = JdbcUtil.getConnection();
		Statement statement = connection.createStatement();
		String sql = "CREATE TABLE IF NOT EXISTS Utilisateur " + " (id_utilisateur  INTEGER  NOT NULL AUTO_INCREMENT, "
				+ " nom VARCHAR(255), " + " prenom VARCHAR(255), " + " nomUtilisateur VARCHAR(255) UNIQUE, "
				+ " mdp VARCHAR(255), " + " telephone VARCHAR(255), " + " mail VARCHAR(255), "
				+ " bureau VARCHAR(255), " + " role VARCHAR(255), " + " PRIMARY KEY (id_utilisateur))";

		statement.executeUpdate(sql);
		connection.close();
	}

	// A supprimer si pas besoin
	public void saveUtilisateur(Utilisateur utilisateur) throws SQLException {
		Connection connection = JdbcUtil.getConnection();
		Statement statement = connection.createStatement();

		String sql = "INSERT INTO Utilisateur (nom , prenom, nomUtilisateur, mdp ,role ) " + "VALUES ('"
				+ utilisateur.getNom() + "' ," + " '" + utilisateur.getPrenom() + "'" + " , '" + utilisateur.getNom()
				+ "'" + " , '" + utilisateur.getMdp() + "'" + " , '" + utilisateur.getNom() + "' )";
		statement.executeUpdate(sql);
		connection.commit();
		connection.close();
	}

	public List<Utilisateur> getAllUtlisateur() throws SQLException, IOException {
		Connection connection = JdbcUtil.getConnection();
		Statement statement = connection.createStatement();

		String sql = "SELECT * FROM Utilisateur";
		List<Utilisateur> utilisateurs = new ArrayList<>();

		ResultSet resultSet = statement.executeQuery(sql);

		while (resultSet.next()) {

			addUtilisateurFromResultSet(utilisateurs, resultSet);

		}
		connection.close();
		return utilisateurs;
	}

	public void addUtilisateur(Utilisateur utilisateur) throws SQLException {
		Connection connection = JdbcUtil.getConnection();
		Statement statement = connection.createStatement();

		String sql = "INSERT INTO Utilisateur (nom , prenom, nomUtilisateur, mdp ,role ) " + "VALUES ('"
				+ utilisateur.getNom() + "' ," + " '" + utilisateur.getPrenom() + "'" + " , '"
				+ utilisateur.getNomUtilisateur() + "'" + " , '" + utilisateur.getMdp() + "'" + " , '"
				+ utilisateur.getUserRole().getRole() + "' )";

		statement.executeUpdate(sql);
		connection.commit();
		connection.close();
	}

	public int getUtilisateurIdByNomUtilisateur(String nomUtilisateur) throws SQLException {
		Connection connection = JdbcUtil.getConnection();
		Statement statement = connection.createStatement();

		String sql = "SELECT * FROM Utilisateur WHERE nomUtilisateur = '" + nomUtilisateur + "'";

		ResultSet resultSet = statement.executeQuery(sql);

		int idRetourne = -1;
		while (resultSet.next()) {
			idRetourne = resultSet.getInt("id_utilisateur");
		}
		connection.close();
		return idRetourne;
	}

	public Utilisateur getUtilisateurById(int id) throws SQLException, IOException {
		Connection connection = JdbcUtil.getConnection();
		Statement statement = connection.createStatement();

		String sql = "SELECT * FROM Utilisateur WHERE id_utilisateur = '" + id + "'";

		ResultSet resultSet = statement.executeQuery(sql);
		List<Utilisateur> utilisateurs = new ArrayList<>();
		while (resultSet.next()) {
			addUtilisateurFromResultSet(utilisateurs, resultSet);
		}
		connection.close();
		return utilisateurs.get(0);
	}

	private void addUtilisateurFromResultSet(List<Utilisateur> utilisateurs, ResultSet resultSet)
			throws SQLException, IOException {
		String nomRs = resultSet.getString("nom");
		String prenom = resultSet.getString("prenom");
		String mdp = resultSet.getString("mdp");
		String nomUtilisateur = resultSet.getString("nomUtilisateur");
		String role = resultSet.getString("role");
		Utilisateur utilisateur = getUtilisateurByRole(nomRs, prenom, mdp, nomUtilisateur, role);
		utilisateurs.add(utilisateur);
	}

	private Utilisateur getUtilisateurByRole(String nomRs, String prenom, String mdp, String nomUtilisateur,
			String role) throws IOException, SQLException {
		Utilisateur utilisateur;
		if (role.equals("client")) {
			utilisateur = new Client(prenom, nomRs, nomUtilisateur, mdp);
		} else {
			utilisateur = new Technicien(prenom, nomRs, nomUtilisateur, mdp);
		}
		return utilisateur;
	}

}
