package com.cranberrysupport.util;

import com.cranberrysupport.model.*;

import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class RequeteDao {
	private UtilisateurDao utilisateurDao = new UtilisateurDao();

	public RequeteDao() throws SQLException {
		creerTableRequete();
	}

	public void creerTableRequete() throws SQLException {
		Connection connection = JdbcUtil.getConnection();
		Statement statement = connection.createStatement();
		String sql = "CREATE TABLE IF NOT EXISTS Requete " + "(id INTEGER not NULL AUTO_INCREMENT, "
				+ " sujet VARCHAR(255), " + " description VARCHAR(255), " + " categorie VARCHAR(255),"
				+ " commentaire VARCHAR(255)," + " statut VARCHAR(255)," + " id_utilisateur INTEGER ,"
				+ " FOREIGN KEY (id_utilisateur) references Utilisateur(id_utilisateur)," + " PRIMARY KEY ( id )) ";

		statement.executeUpdate(sql);
		connection.close();
	}

	public List<Requete> getAllRequete() throws SQLException, IOException {
		Connection connection = JdbcUtil.getConnection();
		Statement statement = connection.createStatement();
		String sql = "SELECT * FROM Requete";
		List<Requete> requetes = new ArrayList<>();
		ResultSet resultSet = statement.executeQuery(sql);

		while (resultSet.next()) {

			getRequeteResultSet(requetes, resultSet);
		}
		connection.close();
		return requetes;
	}

	public List<Requete> getRequeteByNomUtilisateur(String nomUtilisateur) throws SQLException, IOException {
		Connection connection = JdbcUtil.getConnection();
		Statement statement = connection.createStatement();
		int idUtilisateur = utilisateurDao.getUtilisateurIdByNomUtilisateur(nomUtilisateur);
		String sql = "SELECT * FROM Requete WHERE id_utilisateur = '" + idUtilisateur + "'";
		List<Requete> requetes = new ArrayList<>();
		ResultSet resultSet = statement.executeQuery(sql);

		while (resultSet.next()) {


			getRequeteResultSet(requetes, resultSet);
		}
		connection.close();
		return requetes;
	}

	// TODO
    public void assignerRequeteAUtilisateur(String nomUtilisateur, Requete requete) throws SQLException {
    	
    }

    // TODO
	public void assignerRequete(String nomUtilisateur) throws SQLException {

	}

	public void sauvegardeRequete(List<Requete> requetes) throws SQLException {
		for (Requete requete : requetes) {
			sauvegardeRequete(requete);
		}
	}

	public void sauvegardeRequete(Requete requete) throws SQLException {
		// TODO Detruire la table et la reconstruire
		Connection connection = JdbcUtil.getConnection();
		Statement statement = connection.createStatement();

		String sql = "INSERT INTO Requete (sujet , description, categorie, commentaire , statut, id_utilisateur ) "
				+ "VALUES ('" + requete.getSujet() + "' ," + " '" + requete.getDescription() + "'" + " , '"
				+ requete.getCategorie().getValueString() + "'" + " , '" + requete.getListeCommentaireString() + "'"
				+ " , '" + requete.getStatut().getValueString() + "'" + " , '"
				+ utilisateurDao.getUtilisateurIdByNomUtilisateur(requete.getClient().getNomUtilisateur()) + "' )";
		statement.executeUpdate(sql);
		connection.commit();
		connection.close();
	}

	public void updateCommentaire(Requete requete) throws SQLException {
		int requeteId = requete.getId();
		Connection connection = JdbcUtil.getConnection();
		Statement statement = connection.createStatement();

		String sql = "DELETE Requete WHERE id = '" + requeteId + "'";
		statement.executeUpdate(sql);
		connection.commit();
		connection.close();
	}

	private void getRequeteResultSet(List<Requete> requetes, ResultSet resultSet) throws SQLException, IOException {
		int id = resultSet.getInt("id");
		String sujet = resultSet.getString("sujet");
		String description = resultSet.getString("description");
		String categorie = resultSet.getString("categorie");
		String commentaire = resultSet.getString("commentaire");
		String statut = resultSet.getString("statut");
		int utilisateurId = Integer.parseInt(resultSet.getString("id_utilisateur"));
		Utilisateur utilisateur = utilisateurDao.getUtilisateurById(utilisateurId);
		Requete requete = new Requete(sujet, description, utilisateur, getCategorieByString(categorie));
		requete.setId(id);
		requete.setStatut(getStatutByString(statut));
		requete.addCommentaire(commentaire, utilisateur);
		requetes.add(requete);
	}

	private RequeteCategorie getCategorieByString(String categorieString) {

		if (categorieString.equals("poste de travail"))
			return RequeteCategorie.posteDeTravail;

		else if (categorieString.equals("serveur"))
			return RequeteCategorie.serveur;

		else if (categorieString.equals("Service web"))
			return RequeteCategorie.serviceWeb;
		else if (categorieString.equals("Compte usager"))
			return RequeteCategorie.compteUsager;
		else
			return RequeteCategorie.autre;
	}

	private RequeteStatut getStatutByString(String statutString) {

		if (statutString.equals("ouvert"))
			return RequeteStatut.OUVERT;

		else if (statutString.equals("succes"))
			return RequeteStatut.SUCCES;

		else if (statutString.equals("abandon"))
			return RequeteStatut.ABANDON;

		else
			return RequeteStatut.EN_TRAITEMENT;
	}

	public void dropTables() throws SQLException {
		Connection connection = JdbcUtil.getConnection();
		Statement statement = connection.createStatement();
		String sqlDrop = "DROP TABLE Requete";
		statement.executeUpdate(sqlDrop);
		connection.close();
	}

	private String getCommentairesSousFormeDeString(List<Commentaire> requeteCommentaires) {
		String commentaires = "";

		if (requeteCommentaires != null) {
			for (int i = 0; i < requeteCommentaires.size(); i++) {
				commentaires += requeteCommentaires.get(i).getCommentaire();
				commentaires += "&#" + requeteCommentaires.get(i).getAuteur().getNomUtilisateur();
				if (requeteCommentaires.size() - 1 == i) {
					commentaires += "";
				} else {
					commentaires += "/%";
				}
			}
		}
		return commentaires;
	}

}
