package com.cranberrysupport.model;

import lombok.Data;

@Data
public class Commentaire {

    protected String commentaire;
    protected Utilisateur auteur;
    
    Commentaire(String commentaire, Utilisateur auteur) {
        this.commentaire = commentaire;
        this.auteur = auteur;
    }

    public String toString() {
        return getAuteur().getNomUtilisateur() + ": " + getCommentaire() + "\n";
    }
}