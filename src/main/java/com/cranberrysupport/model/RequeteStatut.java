package com.cranberrysupport.model;

public enum RequeteStatut {

	OUVERT("ouvert"), EN_TRAITEMENT("en_traitement"), ABANDON("abandon"), SUCCES("succes");
	String valeur;

    RequeteStatut(String s) {
        this.valeur = s;
    }

    public String getValueString() {
        return valeur;
    }

	public static RequeteStatut fromString(String text) {
		if (text != null) {
			if (RequeteStatut.OUVERT.getValueString().equals(text)) {
				return RequeteStatut.OUVERT;
			} else if (RequeteStatut.EN_TRAITEMENT.getValueString().equals(text)) {
				return RequeteStatut.EN_TRAITEMENT;
			} else if (RequeteStatut.SUCCES.getValueString().equals(text)) {
				return RequeteStatut.SUCCES;
			} else if (RequeteStatut.ABANDON.getValueString().equals(text)) {
				return RequeteStatut.ABANDON;
			}
        }
        return null;
    }
}
