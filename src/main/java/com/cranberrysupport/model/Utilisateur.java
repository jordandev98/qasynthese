package com.cranberrysupport.model;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import lombok.Data;
import lombok.ToString;

@Data
@ToString
public abstract class Utilisateur {

	protected String nom;
	protected String prenom;
	protected String nomUtilisateur;
	protected String mdp;
	protected Integer telephone;
	protected String mail;
	protected String bureau;
	public UserRole userRole;

	public Utilisateur(String nom, String mdp, UserRole role) {
		this.nomUtilisateur = nom;
		this.mdp = mdp;
		this.userRole = role;
	}

	public Utilisateur(String prenom, String nom, String nomUtilisateur, String mdp, UserRole role) {
		this.prenom = prenom;
		this.nom = nom;
		this.nomUtilisateur = nomUtilisateur;
		this.mdp = mdp;
		this.userRole = role;
	}

	public abstract List<Requete> getListStatut(RequeteStatut statut);

	public abstract List<Requete> getListeRequetes() throws IOException, SQLException;

	public abstract void ajoutRequete(Requete nouvelle);

	public abstract List<Requete> getListPerso() throws IOException, SQLException;
}