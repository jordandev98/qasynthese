package com.cranberrysupport.model;

public enum RequeteCategorie {

	posteDeTravail("Poste de travail"), serveur("Serveur"), serviceWeb("Service web"), compteUsager("Compte usager"),
	autre("Autre");
	String value;

	RequeteCategorie(String s) {
		this.value = s;
	}

	public String getValueString() {
		return value;
	}

	public static RequeteCategorie fromString(String text) {
		if (text != null) {
			if (RequeteCategorie.posteDeTravail.getValueString().equals(text)) {
				return RequeteCategorie.posteDeTravail;
			} else if (RequeteCategorie.serveur.getValueString().equals(text)) {
				return RequeteCategorie.serveur;
			} else if (RequeteCategorie.serviceWeb.getValueString().equals(text)) {
				return RequeteCategorie.serviceWeb;
			} else if (RequeteCategorie.compteUsager.getValueString().equals(text)) {
				return RequeteCategorie.compteUsager;
			} else if (RequeteCategorie.autre.getValueString().equals(text)) {
				return RequeteCategorie.autre;
			}
		}
		return null;
	}
}