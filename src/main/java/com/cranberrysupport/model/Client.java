package com.cranberrysupport.model;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.cranberrysupport.util.RequeteDao;

public class Client extends Utilisateur {

	private List<Requete> listeRequetesClient;

	public Client(String nom, String mdp, UserRole role) {
		super(nom, mdp, role);
		listeRequetesClient = new ArrayList<Requete>();
	}

	public Client(String prenom, String nom, String nomUtilisateur, String mdp) {
		super(prenom, nom, nomUtilisateur, mdp, UserRole.CLIENT);
		listeRequetesClient = new ArrayList<Requete>();
	}

	@Override
	public void ajoutRequete(Requete nouvelle) {
		listeRequetesClient.add(nouvelle);
	}

	@Override
	public List<Requete> getListeRequetes() throws SQLException, IOException {
		return new RequeteDao().getRequeteByNomUtilisateur(this.nomUtilisateur);
	}

	@Override
	public ArrayList<Requete> getListPerso() {
		throw new UnsupportedOperationException("Not supported yet.");
	}

	@Override
	public ArrayList<Requete> getListStatut(RequeteStatut statut) {
		throw new UnsupportedOperationException("Not supported yet.");
	}
}