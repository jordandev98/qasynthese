package com.cranberrysupport.model;

import com.cranberrysupport.model.*;
import com.cranberrysupport.util.RequeteDao;

import java.io.*;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class BanqueRequetes {
	private static BanqueRequetes instance = null;
	private List<Requete> listeRequetes;
	private RequeteDao requeteDao;

	private BanqueRequetes() throws  IOException, SQLException {

		requeteDao = new RequeteDao();
		listeRequetes = requeteDao.getAllRequete();
	}

	public static BanqueRequetes getInstance() throws  IOException, SQLException {
		if (instance == null) {
			instance = new BanqueRequetes();
			instance.chargerRequetes();
		}

		return instance;
	}

	public void addNouvelleRequete(Requete requete)  {
		requete.setStatut(RequeteStatut.OUVERT);
		listeRequetes.add(requete);
		requete.getClient().ajoutRequete(requete);
		System.out.println("CLIENT: " + requete.getClient());
		System.out.println(requete);
	}

	public List<Requete> getListRequetes(RequeteStatut statut) {
		List<Requete> retour = new ArrayList<Requete>();
		for (int i = 0; i < listeRequetes.size(); i++) {
			if (listeRequetes.get(i).getStatut() == statut) {
				retour.add(listeRequetes.get(i));
			}
		}

		return retour;
	}

	public void assignerRequete(Technicien technicien, Requete requeteSelectionee) {
		requeteSelectionee.setStatut(RequeteStatut.EN_TRAITEMENT);
		requeteSelectionee.setClient(technicien);
		listeRequetes.removeIf(r -> (r.getId() == requeteSelectionee.getId()));
		listeRequetes.add(requeteSelectionee);
	}

	public Requete getDerniereRequete() {
		return listeRequetes.get(listeRequetes.size() - 1);
	}

	private void chargerRequetes() throws IOException, SQLException {
		listeRequetes = requeteDao.getAllRequete();
	}

	public void saveRequetes() throws SQLException {
		requeteDao.dropTables();
		requeteDao.creerTableRequete();
		requeteDao.sauvegardeRequete(listeRequetes);
	}


	public List<Requete> getRequetesByClient(Client client) {
		return listeRequetes.stream().filter(r -> r.getClient().getNomUtilisateur().equals(client.getNomUtilisateur()))
				.collect(Collectors.toList());
	}

}
