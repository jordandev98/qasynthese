package com.cranberrysupport.model;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;

import com.cranberrysupport.model.BanqueRequetes;
import com.cranberrysupport.model.BanqueUtilisateurs;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Requete {

	private Integer id;
	private String sujet = "";
	private String description = "";
	private File fichier;
	private Integer numero;
	private Utilisateur client;
	private Technicien technicien;
	private RequeteStatut statut;
	private RequeteCategorie categorie;
	private ArrayList<Commentaire> listeCommentaires;
	private Utilisateur tempo;

	private static int numeroRequete = 0;

	public Requete(Integer id, String sujet, String description, Client client, RequeteCategorie categorie)
			throws FileNotFoundException, IOException {
		this.sujet = sujet;
		this.description = description;
		this.client = client;

		numero = numeroRequete++;
		statut = RequeteStatut.OUVERT;
		this.categorie = categorie;
		listeCommentaires = new ArrayList<Commentaire>();
	}

	public Requete(String sujet, String descrip, Utilisateur client, RequeteCategorie categorie)
			throws FileNotFoundException, FileNotFoundException, IOException {
		this.client = client;
		this.sujet = sujet;
		this.description = descrip;
		this.tempo = client;
		if (tempo.getUserRole() == UserRole.CLIENT) {
			client = (Client) tempo;
			tempo = null;
		} else {
			technicien = (Technicien) tempo;
			tempo = null;
		}

		numero = numeroRequete++;
		statut = RequeteStatut.OUVERT;
		this.categorie = categorie;
		listeCommentaires = new ArrayList<Commentaire>();
	}

	public Integer incrementeNo() {
		return ++numero;
	}

	public void uploadFichier() {
		// a faire
	}

	public void finaliser(RequeteStatut fin) {
		this.setStatut(fin);
		this.technicien.ajoutListRequetesFinies(this);
	}

	public void addCommentaire(String message, Utilisateur utilisateur) {
		Commentaire suivant = new Commentaire(message, utilisateur);
		listeCommentaires.add(suivant);
	}

	public String getListeCommentaireString() {
		String commentaireString = "";
		for (Commentaire commentaire : listeCommentaires) {
			commentaireString += commentaire + " , ";
		}
		return commentaireString;
	}

	public void setTechnicien(Technicien technicien) {
		this.technicien = technicien;
		technicien.ajouterRequeteAssignee(this);
	}

	public void setCommentairesFromString(String commentaire) throws IOException, SQLException {
		String[] commentaires = commentaire.split("/%");
		for (int i = 0; i < commentaires.length; i++) {
			String[] commentaireElements = commentaires[i].split("&#");
			this.addCommentaire(commentaireElements[0],
					BanqueUtilisateurs.getUserInstance().chercherUtilisateurByNomUtilisateur(commentaireElements[1]));
		}
	}

}