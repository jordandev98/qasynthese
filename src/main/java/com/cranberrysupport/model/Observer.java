package com.cranberrysupport.model;

public interface Observer {
	void onPostAdd(Requete requete);
}