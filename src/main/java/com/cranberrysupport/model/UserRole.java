package com.cranberrysupport.model;

public enum UserRole {
	CLIENT("client"), TECHNICIEN("technicien");
	
	private String role;
	
	private UserRole(String role) {
		this.role = role;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}
	
	
}
