package com.cranberrysupport.model;

import com.cranberrysupport.util.RequeteDao;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class Technicien extends Utilisateur {

	private List<Requete> listeRequetesTech;
	private List<Requete> ListeRequetesFinies;
	private List<Requete> listeRequetesEnCours;
	private List<Requete> listeRequetesPerso;
	private RequeteDao requeteDao = new RequeteDao();

	Technicien(String nom, String mdp, UserRole role) throws SQLException {
		super(nom, mdp, role);
	}

	public Technicien(String prenom, String nom, String nomUtilisateur, String mdp) throws SQLException, IOException {
		super(prenom, nom, nomUtilisateur, mdp, UserRole.TECHNICIEN);
		listeRequetesTech = new ArrayList<>();
		listeRequetesEnCours = new ArrayList<>();
		ListeRequetesFinies = new ArrayList<>();
		listeRequetesPerso = new ArrayList<>();
	}

	@Override
	public List<Requete> getListeRequetes() throws IOException, SQLException {
		return requeteDao.getRequeteByNomUtilisateur(this.nomUtilisateur);
	}

	// Actions lorsqu'il se fait assigner une requete
	public void ajouterRequeteAssignee(Requete assignee) {
		listeRequetesEnCours.add(assignee);
		listeRequetesTech.add(assignee);
	}

	// Action sur les liste lorsqu'une requete est finalisée
	public void ajoutListRequetesFinies(Requete finie) {
		ListeRequetesFinies.add(finie);
		listeRequetesEnCours.remove(finie);
	}

	// Ajoute une requete (qu'il a lui-même créée)
	@Override
	public void ajoutRequete(Requete nouvelle) {
		listeRequetesPerso.add(nouvelle);
	}

	@Override
	public List<Requete> getListPerso() throws IOException, SQLException {
	   return requeteDao.getRequeteByNomUtilisateur(nomUtilisateur);
	}

	@Override
	public ArrayList<Requete> getListStatut(RequeteStatut statut) {
		ArrayList<Requete> r = new ArrayList<Requete>();
		for (int i = 0; i < listeRequetesTech.size(); i++) {
			if (listeRequetesTech.get(i).getStatut().equals(statut)) {
				r.add(listeRequetesTech.get(i));
			}

		}
		return r;
	}

	// Retourne une string contenant pour ce technicien le nombre
	// de requetes en fonction de leur statut
	public String getRequeteParStatut() {
		String ouvert = "Statut ouvert: ";
		int ouv = 0;
		String enTraitement = "Statut En traitement: ";
		int tr = 0;
		String succes = "Statut succès: ";
		int su = 0;
		String abandon = "Statut abandonnée: ";
		int ab = 0;
		String parStatut = "";
		for (int i = 0; i < listeRequetesPerso.size(); i++) {
			if (listeRequetesPerso.get(i).getStatut().equals(RequeteStatut.OUVERT)) {
				ouv++;
			}
			if (listeRequetesPerso.get(i).getStatut().equals(RequeteStatut.EN_TRAITEMENT)) {
				tr++;
			}
			if (listeRequetesPerso.get(i).getStatut().equals(RequeteStatut.SUCCES)) {
				su++;
			}
			if (listeRequetesPerso.get(i).getStatut().equals(RequeteStatut.ABANDON)) {
				ab++;
			}
		}

		parStatut = ouvert + ouv + "\n" + enTraitement + tr + "\n" + succes + su + "\n" + abandon + ab + "\n";
		return parStatut;

	}

}