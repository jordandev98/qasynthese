package com.cranberrysupport.model;

import java.io.FileNotFoundException;

import java.io.FileReader;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

import com.cranberrysupport.model.Client;
import com.cranberrysupport.model.Technicien;
import com.cranberrysupport.model.UserRole;
import com.cranberrysupport.model.Utilisateur;
import com.cranberrysupport.util.Constants;
import com.cranberrysupport.util.UtilisateurDao;

public class BanqueUtilisateurs {

	private static final String SEPARATEUR_LIGNES_FICHIER = ";";
	private static final int LISTE_UTILISATEURS_TAILLE = 100;
	private static BanqueUtilisateurs listeUsers = null;
	private List<Utilisateur> listeUtilisateurs;
	private  UtilisateurDao utilisateurDao = new UtilisateurDao();

	private BanqueUtilisateurs() throws SQLException, IOException {
		listeUtilisateurs = utilisateurDao.getAllUtlisateur();
	}


	public static BanqueUtilisateurs getUserInstance() throws IOException, SQLException {
		if (listeUsers == null) {
			listeUsers = new BanqueUtilisateurs();
		}

		return listeUsers;
	}

	public List<Utilisateur> getListUtilisateurs() {
		return listeUtilisateurs;
	}

	public List<Utilisateur> getListUtilisateurs(UserRole userRole) {
		List<Utilisateur> users = new ArrayList<Utilisateur>(LISTE_UTILISATEURS_TAILLE);
		for (int i = 0; i < listeUtilisateurs.size(); i++) {
			if (listeUtilisateurs.get(i).getUserRole().equals(userRole)) {
				users.add(listeUtilisateurs.get(i));
			}
		}
		return users;
	}

	public Utilisateur chercherUtilisateurByNomUtilisateur(String nomUtilisateur) {
		for (int i = 0; i < listeUtilisateurs.size(); i++) {
			if (listeUtilisateurs.get(i).getNomUtilisateur().equalsIgnoreCase(nomUtilisateur)) {
				return listeUtilisateurs.get(i);
			}
		}
		return null;
	}

	private void addUtilisateur(String prenom, String nom, String nomUtilisateur, String mdp, String role) throws IOException, SQLException {
		UserRole userRole = Arrays.asList(UserRole.values()).stream().filter(u -> u.getRole().equals(role)).findFirst()
				.get();
		Utilisateur utilisateur = null;
		switch (userRole) {
		case TECHNICIEN:
			utilisateur = new Technicien(prenom, nom, nomUtilisateur, mdp);
			break;
		case CLIENT:
		default:
			utilisateur = new Client(prenom, nom, nomUtilisateur, mdp);
			break;
		}

		listeUtilisateurs.add(utilisateur);
	}
}