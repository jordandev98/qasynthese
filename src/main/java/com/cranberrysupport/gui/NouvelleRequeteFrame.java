package com.cranberrysupport.gui;

import com.cranberrysupport.model.BanqueRequetes;
import com.cranberrysupport.model.*;
import com.cranberrysupport.service.FrameService;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.io.*;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class NouvelleRequeteFrame extends Frame {

    private static final String SELECTIONNER_FICHIER_BTN_TEXTE = "Téléverser un fichier";
    private static final String FICHIER_LABEL = "Fichier:";
    private static final String DESCRIPTION_LABEL = "Description:";
    private static final String SUJET_LABEL = "Sujet de la requête:";
    private static final String TITRE_LABEL = "Nouvelle requête";
    private static final String QUITTER_BTN_TEXTE = "Revenir";
    private static final String CATEGORIE_LABEL = "Catégorie:";
    private static final String TERMINER_BTN_TEXTE = "Terminer";

    private static final long serialVersionUID = -7437769255462153082L;

    private Utilisateur utilisateur;
    private JFrame pagePrecedente;
    private File fichier;
    private String[] categories = new String[]{"Poste de travail", "Serveur", "Service web", "Compte usager",
            "Autre"};

    private JComboBox categoriesComboBox;

    private JLabel categorieLabel;
    private JLabel descriptionLabel;
    private JLabel fichierLabel;
    private JLabel sujetLabel;
    private JLabel titreLabel;

    private JTextArea descpriptionArea;

    private JScrollPane descpScroll;

    private JButton terminerBtn;
    private JButton quitterBtn;
    private JButton selectionnerFichierBtn;

    private JFileChooser fileChooser;

    private JSeparator jSeparator;

    private JTextField pathTextField;
    private JTextField sujetTextField;

    public NouvelleRequeteFrame(Utilisateur utilisateur, JFrame pagePrecedente) {
        initComponents();
        this.setVisible(true);
        FrameService.centerFrame(this);

        this.utilisateur = utilisateur;
        this.pagePrecedente = pagePrecedente;
    }

    private void initComponents() {
        fileChooser = new JFileChooser();
        titreLabel = new JLabel();
        sujetLabel = new JLabel();
        sujetTextField = new JTextField();
        descriptionLabel = new JLabel();
        descpScroll = new JScrollPane();
        descpriptionArea = new JTextArea();
        fichierLabel = new JLabel();
        pathTextField = new JTextField();
        selectionnerFichierBtn = new JButton();
        categoriesComboBox = new JComboBox();
        categorieLabel = new JLabel();
        terminerBtn = new JButton();
        quitterBtn = new JButton();
        jSeparator = new JSeparator();

        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

        setLabels();
        setTexteBoutons();
        setListeners();

        setProprietesDescriptionArea();
        descpScroll.setViewportView(descpriptionArea);

        setModelCategoriesBox();

        GroupLayout layout = new GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        setHorizontalGroup(layout);
        setVerticalGroup(layout);
        pack();
    }

    private void setModelCategoriesBox() {
        categoriesComboBox.setModel(new DefaultComboBoxModel(categories));
    }

    private void setListeners() {
        setListenersBoutons();
        setTextFieldsActionListeners();
        setFileChooserFocuseListener();
        setActionListenerCategoriesComboBox();
    }

    private void setActionListenerCategoriesComboBox() {
        categoriesComboBox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                catBoxActionPerformed(evt);
            }
        });
    }

    private void setFileChooserFocuseListener() {
        fileChooser.addFocusListener(new FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                fileChooserFocusGained(evt);
            }
        });
    }

    private void setTextFieldsActionListeners() {
        pathTextField.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                displayPathFichier(evt);
            }
        });

        sujetTextField.addActionListener(new ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                sujetFldActionPerformed(evt);
            }
        });
    }

    private void setProprietesDescriptionArea() {
        descpriptionArea.setColumns(20);
        descpriptionArea.setRows(5);
        descpriptionArea.setBorder(null);
    }

    private void setListenersBoutons() {
        terminerBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                doneBtnActionPerformed(evt);
            }
        });

        quitterBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                quitBtnActionPerformed(evt);
            }
        });

        selectionnerFichierBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                selectionnerFichierBtnActionPerformed(evt);
            }
        });

    }

    private void setTexteBoutons() {
        selectionnerFichierBtn.setText(SELECTIONNER_FICHIER_BTN_TEXTE);
        terminerBtn.setText(TERMINER_BTN_TEXTE);
        quitterBtn.setText(QUITTER_BTN_TEXTE);
    }

    private void setLabels() {
        titreLabel.setText(TITRE_LABEL);
        sujetLabel.setText(SUJET_LABEL);
        descriptionLabel.setText(DESCRIPTION_LABEL);
        fichierLabel.setText(FICHIER_LABEL);
        categorieLabel.setText(CATEGORIE_LABEL);
    }

    private void setVerticalGroup(javax.swing.GroupLayout layout) {
        layout.setVerticalGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addGroup(layout
                .createSequentialGroup().addContainerGap().addComponent(titreLabel).addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(sujetLabel).addComponent(sujetTextField, javax.swing.GroupLayout.PREFERRED_SIZE,
                                javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(descriptionLabel).addComponent(descpScroll,
                                javax.swing.GroupLayout.PREFERRED_SIZE, 105, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(pathTextField, javax.swing.GroupLayout.PREFERRED_SIZE,
                                javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(fichierLabel))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(selectionnerFichierBtn).addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(categoriesComboBox, javax.swing.GroupLayout.PREFERRED_SIZE,
                                javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(categorieLabel))
                .addGap(27, 27, 27)
                .addComponent(jSeparator, javax.swing.GroupLayout.PREFERRED_SIZE, 8,
                        javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(quitterBtn).addComponent(terminerBtn))
                .addContainerGap(31, Short.MAX_VALUE)));
    }

    private void setHorizontalGroup(javax.swing.GroupLayout layout) {
        layout.setHorizontalGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addGroup(layout
                .createSequentialGroup().addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(layout.createSequentialGroup().addGroup(layout
                                .createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addComponent(titreLabel)
                                .addGroup(layout.createSequentialGroup()
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                .addComponent(sujetLabel).addComponent(descriptionLabel)
                                                .addComponent(fichierLabel))
                                        .addGap(18, 18, 18)
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                .addComponent(pathTextField, javax.swing.GroupLayout.DEFAULT_SIZE, 246,
                                                        Short.MAX_VALUE)
                                                .addComponent(sujetTextField, javax.swing.GroupLayout.DEFAULT_SIZE, 246,
                                                        Short.MAX_VALUE)
                                                .addComponent(descpScroll, javax.swing.GroupLayout.DEFAULT_SIZE, 246,
                                                        Short.MAX_VALUE)
                                                .addComponent(selectionnerFichierBtn))))
                                .addContainerGap(30, Short.MAX_VALUE))
                        .addGroup(layout.createSequentialGroup().addComponent(categorieLabel).addGap(63, 63, 63)
                                .addComponent(categoriesComboBox, javax.swing.GroupLayout.PREFERRED_SIZE,
                                        javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addContainerGap(176, Short.MAX_VALUE))
                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING,
                                layout.createSequentialGroup().addGap(124, 124, 124).addComponent(terminerBtn)
                                        .addGap(18, 18, 18).addComponent(quitterBtn)
                                        .addContainerGap(104, Short.MAX_VALUE))))
                .addComponent(jSeparator, javax.swing.GroupLayout.DEFAULT_SIZE, 400, Short.MAX_VALUE));
    }

    private void sujetFldActionPerformed(ActionEvent evt) {
        // TODO add your handling code here:
    }

    private void displayPathFichier(java.awt.event.ActionEvent evt) {
        pathTextField.setText(fileChooser.getSelectedFile().getPath());
    }

    private void selectionnerFichierBtnActionPerformed(ActionEvent evt) {
        fileChooser.showOpenDialog(pagePrecedente);

        displayPathFichier(evt);
        try {
            fichier = new File("dat/" + fileChooser.getSelectedFile().getName());
            InputStream srcFile = new FileInputStream(fileChooser.getSelectedFile());
            OutputStream newFile = new FileOutputStream(fichier);
            byte[] buf = new byte[4096];
            int len;
            while ((len = srcFile.read(buf)) > 0) {
                newFile.write(buf, 0, len);
            }
            srcFile.close();
            newFile.close();
        } catch (IOException ex) {
            Logger.getLogger(NouvelleRequeteFrame.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void catBoxActionPerformed(java.awt.event.ActionEvent evt) {
    }


    private void doneBtnActionPerformed(java.awt.event.ActionEvent evt) {
        try {
            BanqueRequetes.getInstance().addNouvelleRequete(makeRequete());
            BanqueRequetes.getInstance().getDerniereRequete().setFichier(fichier);

            this.setVisible(false);

            if (utilisateur.getUserRole() == UserRole.TECHNICIEN)
                pagePrecedente = new TechnicienLoggedFrame((Technicien) utilisateur);
			else
				pagePrecedente = new ClientLoggedFrame((Client) utilisateur);

            pagePrecedente.setVisible(true);

        } catch (FileNotFoundException ex) {
            Logger.getLogger(NouvelleRequeteFrame.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(NouvelleRequeteFrame.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private Requete makeRequete() throws FileNotFoundException, IOException {
        return new Requete(sujetTextField.getText(), descpriptionArea.getText().replaceAll("\n", " "), utilisateur,
                RequeteCategorie.fromString((String) categoriesComboBox.getSelectedItem()));
    }

    private void quitBtnActionPerformed(java.awt.event.ActionEvent evt) {
        this.setVisible(false);
        pagePrecedente.setVisible(true);

    }

    private void fileChooserFocusGained(FocusEvent evt) {
        System.out.println("FOCUS GAINED");
    }
}
