package com.cranberrysupport.gui;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.DefaultListModel;
import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import com.cranberrysupport.controller.RequeteControleur;
import com.cranberrysupport.model.BanqueRequetes;
import com.cranberrysupport.model.Client;
import com.cranberrysupport.model.Commentaire;
import com.cranberrysupport.model.Requete;
import com.cranberrysupport.model.Utilisateur;
import com.cranberrysupport.service.FrameService;

public class ClientLoggedFrame extends Frame {

	private static final long serialVersionUID = -5609471414917299833L;

	private static final String AUCUNE_REQUETE_MESSAGE = "Vous n'avez pas de requête encore.";
	private static final String FICHIER_BTN_TEXT = "Ajouter un fichier";
	private static final String VOIR_COMMENTAIRES_BTN_TEXTE = "Voir les commentaires";
	private static final String MODIF_BTN_TEXT = "Ajouter un commentaire";
	private static final String ADD_REQUETE_BTN_TEXT = "Faire une nouvelle requête";
	private static final String REQUETES_LABEL = "Vos requêtes:";

	private Utilisateur client;
	private List<Requete> listeRequetes;
	private Requete requete;
	private JFrame prec;
	private File file;
	private boolean isShowComments = false;

	private JButton ajouterRequeteBtn;
	private JButton fichierBtn;
	private JButton modifBtn;
	private JButton quitterBtn;
	private JButton voirCommentairesBtn;

	private JTextArea commentArea;
	private JTextArea requeteArea;

	private JFileChooser fileChooser;

	private JLabel jLabel1;
	private JLabel requetesLbl;

	private JScrollPane jScrollPane1;
	private JScrollPane jScrollPane2;
	private JScrollPane jScrollPane3;

	private JList<String> listRequetes;

	private RequeteControleur requeteControleur = new RequeteControleur();

	public ClientLoggedFrame(Utilisateur client) throws IOException, SQLException {
		initComponents();
		this.setVisible(true);
		FrameService.centerFrame(this);

		this.client = client;
		listeRequetes = requeteControleur.getRequetesByClient((Client) client);
		DefaultListModel<String> requetesListModel = initialiserListeRequetes(client);
		peindreListeRequetes(requetesListModel);
		commentArea.setVisible(false);
	}

	private void peindreListeRequetes(DefaultListModel<String> requetesListModel) {
		listRequetes.setModel(requetesListModel);
	}

	private DefaultListModel<String> initialiserListeRequetes(Utilisateur client) throws IOException, SQLException {
		DefaultListModel<String> listModelRequetes = new DefaultListModel<String>();
		List<Requete> temp = requeteControleur.getRequetesByClient((Client) client);
		if (temp.isEmpty()) {
			listModelRequetes.addElement(AUCUNE_REQUETE_MESSAGE);
		} else {
			for (int i = 0; i < temp.size(); i++) {
				listModelRequetes.addElement(temp.get(i).getSujet());
			}
		}
		return listModelRequetes;
	}

	private void initComponents() {
		fileChooser = new JFileChooser();
		jScrollPane1 = new JScrollPane();
		listRequetes = new JList<String>();
		requetesLbl = new JLabel();
		ajouterRequeteBtn = new JButton();
		quitterBtn = new JButton();
		modifBtn = new JButton();
		fichierBtn = new JButton();
		jLabel1 = new JLabel();
		jScrollPane2 = new JScrollPane();
		requeteArea = new JTextArea();
		voirCommentairesBtn = new JButton();
		jScrollPane3 = new JScrollPane();
		commentArea = new JTextArea();

		setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

		listRequetes.addListSelectionListener(new ListSelectionListener() {
			public void valueChanged(ListSelectionEvent evt) {
				listRequeteListValueChanged(evt);
			}
		});
		jScrollPane1.setViewportView(listRequetes);

		requetesLbl.setText(REQUETES_LABEL);

		ajouterRequeteBtn.setText(ADD_REQUETE_BTN_TEXT);
		ajouterRequeteBtn.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				addRequeteBtnActionPerformed(evt);
			}
		});

		quitterBtn.setText("Quitter");
		quitterBtn.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				quitterBtnActionPerformed(evt);
			}
		});

		modifBtn.setText(MODIF_BTN_TEXT);
		modifBtn.setEnabled(false);
		modifBtn.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				modifBtnActionPerformed(evt);
			}
		});

		fichierBtn.setText(FICHIER_BTN_TEXT);
		fichierBtn.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				fichierBtnActionPerformed(evt);
			}
		});

		jLabel1.setText("Modifier la requête sélectionnée:");

		requeteArea.setColumns(20);
		requeteArea.setEditable(false);
		requeteArea.setLineWrap(true);
		requeteArea.setRows(5);
		jScrollPane2.setViewportView(requeteArea);

		voirCommentairesBtn.setBackground(new Color(102, 153, 255));
		voirCommentairesBtn.setText(VOIR_COMMENTAIRES_BTN_TEXTE);
		voirCommentairesBtn.addActionListener(new ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				voirComsBtnActionPerformed(evt);
			}
		});

		commentArea.setColumns(20);
		commentArea.setLineWrap(true);
		commentArea.setRows(5);
		commentArea.addKeyListener(new KeyAdapter() {
			public void keyPressed(KeyEvent evt) {
				commentAreaKeyPressed(evt);
			}
		});
		jScrollPane3.setViewportView(commentArea);

		GroupLayout layout = new GroupLayout(getContentPane());
		getContentPane().setLayout(layout);
		setHorizontalGroup(layout);
		setVerticalGroup(layout);
		pack();
	}

	private void setVerticalGroup(GroupLayout layout) {
		layout.setVerticalGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(
				GroupLayout.Alignment.TRAILING,
				layout.createSequentialGroup().addGroup(layout.createParallelGroup(GroupLayout.Alignment.TRAILING)
						.addGroup(GroupLayout.Alignment.LEADING,
								layout.createSequentialGroup().addGap(76, 76, 76).addComponent(jScrollPane2,
										javax.swing.GroupLayout.DEFAULT_SIZE, 324, Short.MAX_VALUE))
						.addGroup(layout.createSequentialGroup().addGap(22, 22, 22).addComponent(ajouterRequeteBtn)
								.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
								.addComponent(requetesLbl)
								.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
								.addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
										.addGroup(layout.createSequentialGroup()
												.addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 290,
														Short.MAX_VALUE)
												.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
												.addComponent(quitterBtn))
										.addGroup(layout.createSequentialGroup().addComponent(voirCommentairesBtn)
												.addGap(26, 26, 26).addComponent(jLabel1)
												.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
												.addComponent(fichierBtn)
												.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
												.addComponent(modifBtn)
												.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
												.addComponent(jScrollPane3, javax.swing.GroupLayout.DEFAULT_SIZE, 197,
														Short.MAX_VALUE)))))
						.addContainerGap()));
	}

	private void setHorizontalGroup(javax.swing.GroupLayout layout) {
		layout.setHorizontalGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addGroup(layout
				.createSequentialGroup().addContainerGap()
				.addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
						.addComponent(ajouterRequeteBtn).addComponent(requetesLbl)
						.addGroup(layout.createSequentialGroup()
								.addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
										.addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 214,
												javax.swing.GroupLayout.PREFERRED_SIZE)
										.addComponent(quitterBtn))
								.addGap(10, 10, 10)
								.addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
										.addComponent(voirCommentairesBtn).addComponent(fichierBtn)
										.addComponent(modifBtn).addComponent(jLabel1).addComponent(jScrollPane3,
												javax.swing.GroupLayout.PREFERRED_SIZE, 166,
												javax.swing.GroupLayout.PREFERRED_SIZE))
								.addGap(9, 9, 9).addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 270,
										Short.MAX_VALUE)))
				.addContainerGap()));
	}

	private void addRequeteBtnActionPerformed(ActionEvent evt) {
		this.setVisible(false);
		new NouvelleRequeteFrame(client, this);
	}

	private void listRequeteListValueChanged(ListSelectionEvent evt) {
		updateRequete();
	}

	private void quitterBtnActionPerformed(ActionEvent evt) {
		enregistrerRequetesEtQuitter();
	}

	private void enregistrerRequetesEtQuitter() {
		try {
			this.setVisible(false);
			BanqueRequetes.getInstance().saveRequetes();
			System.exit(0);
		} catch (IOException | SQLException ex) {
			Logger.getLogger(ClientLoggedFrame.class.getName()).log(Level.SEVERE, null, ex);
		}
	}

	private void fichierBtnActionPerformed(ActionEvent evt) {
		fileChooser.showOpenDialog(prec);
		file = fileChooser.getSelectedFile();
		try {
			listeRequetes.get(listRequetes.getSelectedIndex()).setFichier(file);
		} catch (IndexOutOfBoundsException e) {
		}

		sauvegarderFichierDansProjet();
	}

	private void sauvegarderFichierDansProjet() {
		File f = fileChooser.getSelectedFile();
		if (f != null) {
			try {
				FileWriter newFile = new FileWriter("dat/fichiers/" + f.getName());
			} catch (IOException ex) {
				Logger.getLogger(NouvelleRequeteFrame.class.getName()).log(Level.SEVERE, null, ex);
			}
			if (file.exists()) {
				FichierAddOkFrame okFrame = new FichierAddOkFrame();
				okFrame.setVisible();
			}
		}
	}

	private void modifBtnActionPerformed(ActionEvent evt) {
		commentArea.setVisible(true);
		commentArea.requestFocus();
	}

	private void voirComsBtnActionPerformed(ActionEvent evt) {
		afficherCommentaires();
	}

	private void afficherCommentaires() {
		if (requete != null) {
			if (!isShowComments) {
				String s = "";
				for (Commentaire c : requete.getListeCommentaires()) {
					s += c.toString();
				}
				requeteArea.setText(s);
				isShowComments = true;
				voirCommentairesBtn.setText("Voir la rêquete");
				fichierBtn.setEnabled(false);
				modifBtn.setEnabled(true);
			} else {
				updateRequete();
			}
		}
	}

	private void updateRequete() {
		miseAJourRegionAffichageRequete();
	}

	private void miseAJourRegionAffichageRequete() {
		requete = listeRequetes.get(listRequetes.getSelectedIndex());
		requeteArea.setText("Sujet: " + requete.getSujet() + "\nDescription: " + requete.getDescription()
				+ "\nCatégorie: " + requete.getCategorie().toString() + "\nStatut: "
				+ requete.getStatut().toString());
		isShowComments = false;
		voirCommentairesBtn.setText(VOIR_COMMENTAIRES_BTN_TEXTE);
		fichierBtn.setEnabled(true);
		modifBtn.setEnabled(false);
	}

	private void commentAreaKeyPressed(KeyEvent evt) {
		if (evt.getKeyCode() == java.awt.event.KeyEvent.VK_ENTER && !commentArea.getText().isEmpty()) {
			ajouterCommentaire();
		}
	}

	private void ajouterCommentaire() {
		requete.addCommentaire(commentArea.getText(), client);
		String s = "";
		for (Commentaire c : requete.getListeCommentaires()) {
			s += c.toString();
		}
		requeteArea.setText(s);
		commentArea.setVisible(false);
		modifBtn.requestFocus();
	}
}
