package com.cranberrysupport.gui;

import com.cranberrysupport.model.UserRole;
import com.cranberrysupport.service.FrameService;

public class ErrorLoginFrame extends Frame {

	private static final String REVENIR_BTN_TEXT = "Recommencer";
	private static final String ERREUR_LABEL_TEXT = "Désolé, votre nom d'utilisateur ou votre mot de passe est incorrect.";
	private static final long serialVersionUID = -6875087231530755686L;
	private javax.swing.JLabel erreurLabel;
	private javax.swing.JButton revenirLbl;

	private UserRole userRole;

	public ErrorLoginFrame(UserRole userRole) {
		initComponents();
		this.userRole = userRole;
		this.setVisible(true);
		FrameService.centerFrame(this);
	}

	private void initComponents() {

		erreurLabel = new javax.swing.JLabel();
		revenirLbl = new javax.swing.JButton();

		setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

		erreurLabel.setText(ERREUR_LABEL_TEXT);

		revenirLbl.setText(REVENIR_BTN_TEXT);
		revenirLbl.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseClicked(java.awt.event.MouseEvent evt) {
				revenirLblMouseClicked(evt);
			}
		});

		javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
		getContentPane().setLayout(layout);
		buildGroupLayout(layout);

		pack();
	}

	private void buildGroupLayout(javax.swing.GroupLayout layout) {
		layout.setHorizontalGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
				.addGroup(javax.swing.GroupLayout.Alignment.TRAILING,
						layout.createSequentialGroup().addContainerGap(81, Short.MAX_VALUE).addComponent(erreurLabel)
								.addGap(72, 72, 72))
				.addGroup(layout.createSequentialGroup().addGap(184, 184, 184).addComponent(revenirLbl)
						.addContainerGap(194, Short.MAX_VALUE)));
		layout.setVerticalGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
				.addGroup(layout.createSequentialGroup().addGap(24, 24, 24).addComponent(erreurLabel).addGap(18, 18, 18)
						.addComponent(revenirLbl).addContainerGap(27, Short.MAX_VALUE)));
	}

	private void revenirLblMouseClicked(java.awt.event.MouseEvent evt) {
		this.setVisible(false);
		new UtilisateurLoginFrame(userRole);
	}
}
