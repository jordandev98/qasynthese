package com.cranberrysupport.gui;

import javax.swing.GroupLayout;

import com.cranberrysupport.model.UserRole;
import com.cranberrysupport.service.FrameService;

public class CranberryFrame extends Frame {

	private static final long serialVersionUID = 1L;
	
	private static final String CLIENT_BTN_TEXTE = "Client";
	private static final String TECHNICIEN_BTN_TEXTE = "Technicien";
	private static final String CRANBERRY_FRAME_LABEL = "Démarrer l'application en tant que:";
	private static final String CRANBERRY_FRAME_NAME = "demarrage";

	private javax.swing.JFrame jFrame;
	private javax.swing.JButton clientBtn;
	private javax.swing.JButton technicienBtn;
	private javax.swing.JLabel cranberryFrameLabel;

	public CranberryFrame() {
		initComponents();
		FrameService.centerFrame(this);
	}

	private void initComponents() {
		jFrame = new javax.swing.JFrame();
		cranberryFrameLabel = new javax.swing.JLabel();
		technicienBtn = new javax.swing.JButton();
		clientBtn = new javax.swing.JButton();

		javax.swing.GroupLayout jFrameLayout = new javax.swing.GroupLayout(jFrame.getContentPane());
		jFrame.getContentPane().setLayout(jFrameLayout);

		setProprietesJFrameLayout(jFrameLayout);

		setProprietesFrame();

		GroupLayout layout = new GroupLayout(getContentPane());
		getContentPane().setLayout(layout);

		buildGroupLayout(layout);
		pack();
	}

	private void buildGroupLayout(GroupLayout layout) {
		layout.setVerticalGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
				.addGroup(layout.createSequentialGroup().addContainerGap()
						.addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
								.addComponent(cranberryFrameLabel).addComponent(clientBtn).addComponent(technicienBtn))
						.addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)));

		layout.setHorizontalGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
				.addGroup(layout.createSequentialGroup().addGap(32, 32, 32).addComponent(cranberryFrameLabel)
						.addGap(35, 35, 35).addComponent(clientBtn).addGap(18, 18, 18).addComponent(technicienBtn)
						.addContainerGap(42, Short.MAX_VALUE)));
	}

	private void setProprietesFrame() {
		setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
		setBackground(new java.awt.Color(255, 255, 255));
		setName(CRANBERRY_FRAME_NAME);
		cranberryFrameLabel.setText(CRANBERRY_FRAME_LABEL);
		setTextButtons();
		setListenersButtons();
	}

	private void setTextButtons() {
		technicienBtn.setText(TECHNICIEN_BTN_TEXTE);
		clientBtn.setText(CLIENT_BTN_TEXTE);
	}

	private void setListenersButtons() {
		clientBtn.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseClicked(java.awt.event.MouseEvent evt) {
				clientBtnMouseClicked(evt);
			}
		});

		technicienBtn.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseClicked(java.awt.event.MouseEvent evt) {
				technicienBtnMouseClicked(evt);
			}
		});
	}

	private void setProprietesJFrameLayout(javax.swing.GroupLayout jFrameLayout) {
		jFrameLayout.setHorizontalGroup(jFrameLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
				.addGap(0, 400, Short.MAX_VALUE));
		jFrameLayout.setVerticalGroup(jFrameLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
				.addGap(0, 300, Short.MAX_VALUE)); 
	}

	private void technicienBtnMouseClicked(java.awt.event.MouseEvent evt) {
		this.setVisible(false);
		new UtilisateurLoginFrame(UserRole.TECHNICIEN);
	}

	private void clientBtnMouseClicked(java.awt.event.MouseEvent evt) {
		this.setVisible(false);
		new UtilisateurLoginFrame(UserRole.CLIENT);
	}
}