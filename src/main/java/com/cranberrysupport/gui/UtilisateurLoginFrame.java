package com.cranberrysupport.gui;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.WindowConstants;

import com.cranberrysupport.model.BanqueUtilisateurs;
import com.cranberrysupport.model.UserRole;
import com.cranberrysupport.model.Utilisateur;
import com.cranberrysupport.service.FrameService;

public class UtilisateurLoginFrame extends Frame {
	private static final String PLACEHOLDER_MOT_DE_PASSE_TEXTFIELD = "ex.: MonMdp";
	private static final String OK_BTN_TEXT = "OK";
	private static final String PLACEHOLDER_NOM_UTILISATEUR_TEXTFIELD = "ex.: Isabeau Desrochers";
	private static final String MOT_DE_PASSE_TEXFIELD_LABEL = "Mot de passe:";
	private static final String NOM_UTILISATEUR_TEXTFIELD_LABEL = "Nom d'utilisateur:";
	private static final String CANCEL_BUTTON_CLIENT_LOGIN_FRAME = "Annuler";
	private static final long serialVersionUID = -7867620135330233601L;

	private Utilisateur utilisateur;

	private javax.swing.JLabel exMdp;
	private javax.swing.JLabel exNom;
	private javax.swing.JLabel mdpLbl;
	private javax.swing.JLabel nomUtilisateurLbl;

	private javax.swing.JButton okBtn;
	private javax.swing.JButton annulerBtn;

	private javax.swing.JTextField nomUtilisateurTextField;
	private javax.swing.JTextField motDePasseTextField;

	private UserRole userRole;

	public UtilisateurLoginFrame(UserRole userRole) {
		initComponents();
		setVisible(true);
		FrameService.centerFrame(this);
		this.userRole = userRole;
	}

	private void initComponents() {
		nomUtilisateurLbl = new JLabel();
		mdpLbl = new JLabel();

		exNom = new JLabel();
		exMdp = new JLabel();

		nomUtilisateurTextField = new JTextField();
		motDePasseTextField = new JTextField();

		nomUtilisateurTextField.setText("techguest");
		motDePasseTextField.setText("password");

		okBtn = new JButton();
		annulerBtn = new JButton();

		setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

		nomUtilisateurLbl.setText(NOM_UTILISATEUR_TEXTFIELD_LABEL);

		mdpLbl.setText(MOT_DE_PASSE_TEXFIELD_LABEL);

		exNom.setText(PLACEHOLDER_NOM_UTILISATEUR_TEXTFIELD);

		exMdp.setText(PLACEHOLDER_MOT_DE_PASSE_TEXTFIELD);

		okBtn.setText(OK_BTN_TEXT);
		okBtn.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseClicked(java.awt.event.MouseEvent evt) {
				okBtnMouseClicked(evt);
			}
		});

		annulerBtn.setText(CANCEL_BUTTON_CLIENT_LOGIN_FRAME);
		annulerBtn.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseClicked(java.awt.event.MouseEvent evt) {
				annulerBtnMouseClicked(evt);
			}
		});

		javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
		getContentPane().setLayout(layout);
		buildGroupLayout(layout);

		pack();
	}

	private void buildGroupLayout(javax.swing.GroupLayout layout) {
		layout.setHorizontalGroup(
				layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
						.addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
								.addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
										.addGroup(layout.createSequentialGroup().addContainerGap()
												.addComponent(okBtn, javax.swing.GroupLayout.PREFERRED_SIZE, 69,
														javax.swing.GroupLayout.PREFERRED_SIZE)
												.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
												.addComponent(annulerBtn))
										.addGroup(layout.createSequentialGroup().addGap(37, 37, 37).addGroup(
												layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
														.addComponent(nomUtilisateurLbl).addComponent(mdpLbl))
												.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
												.addGroup(layout
														.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
														.addComponent(motDePasseTextField,
																javax.swing.GroupLayout.DEFAULT_SIZE, 149,
																Short.MAX_VALUE)
														.addComponent(nomUtilisateurTextField,
																javax.swing.GroupLayout.DEFAULT_SIZE, 149,
																Short.MAX_VALUE))))
								.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
								.addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
										.addComponent(exMdp).addComponent(exNom))
								.addContainerGap()));

		layout.setVerticalGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
				.addGroup(layout.createSequentialGroup().addGap(22, 22, 22)
						.addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
								.addComponent(nomUtilisateurLbl)
								.addComponent(nomUtilisateurTextField, javax.swing.GroupLayout.PREFERRED_SIZE,
										javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
								.addComponent(exNom))
						.addGap(18, 18, 18)
						.addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
								.addComponent(mdpLbl)
								.addComponent(motDePasseTextField, javax.swing.GroupLayout.PREFERRED_SIZE,
										javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
								.addComponent(exMdp))
						.addGap(18, 18, 18)
						.addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
								.addComponent(annulerBtn).addComponent(okBtn))
						.addContainerGap(20, Short.MAX_VALUE)));
	}

	private void okBtnMouseClicked(java.awt.event.MouseEvent evt) {
		validateUserInfos();
	}

	private void validateUserInfos() {
		try {
			// A REFACTOR - Utiliser ServiceUtilisateur
			String nomUtilisateur = nomUtilisateurTextField.getText();
			String motDePasse = motDePasseTextField.getText();

			utilisateur = BanqueUtilisateurs.getUserInstance().chercherUtilisateurByNomUtilisateur(nomUtilisateur);

			if (utilisateur == null) {
				new ErrorLoginFrame(userRole);
				this.setVisible(false);
				return;
			}

			this.setVisible(false);
			if (utilisateur.getUserRole() == UserRole.TECHNICIEN && userRole == UserRole.TECHNICIEN
					&& utilisateur.getMdp().equals(motDePasse)) {
				new TechnicienLoggedFrame(utilisateur);
			} else if (utilisateur.getUserRole() == UserRole.CLIENT && userRole == UserRole.CLIENT
					&& utilisateur.getMdp().equals(motDePasse)) {
				new ClientLoggedFrame(utilisateur);
			} else {
				new ErrorLoginFrame(userRole);
			}

		} catch (FileNotFoundException ex) {
			Logger.getLogger(UtilisateurLoginFrame.class.getName()).log(Level.SEVERE, null, ex);
		} catch (IOException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	private void annulerBtnMouseClicked(java.awt.event.MouseEvent evt) {
		this.setVisible(false);
		new CranberryFrame();
	}

}
