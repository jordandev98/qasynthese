package com.cranberrysupport.gui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import com.cranberrysupport.model.BanqueUtilisateurs;
import com.cranberrysupport.model.BanqueRequetes;
import com.cranberrysupport.model.Commentaire;
import com.cranberrysupport.model.Requete;
import com.cranberrysupport.model.RequeteCategorie;
import com.cranberrysupport.model.RequeteStatut;
import com.cranberrysupport.model.Technicien;
import com.cranberrysupport.model.UserRole;
import com.cranberrysupport.model.Utilisateur;
import com.cranberrysupport.service.FrameService;
import com.cranberrysupport.service.ServiceTechnicien;
import com.cranberrysupport.util.RequeteDao;

public class TechnicienLoggedFrame extends Frame {

	private static final String PRENDRE_REQUETE_BTN_TEXTE = "Prendre la requête sélectionnée";
	private static final String REQUETES_DISPONIBLES_LABEL = "Requêtes disponibles non-assignée:";
	private static final String[] CHOIX_ACTIONS_REQUETES = new String[] { "Finaliser", "Succès", "Abandon" };
	private static final String NOUVELLE_REQUÊTE_BTN_TEXTE = "Créer une nouvelle requête";
	private static final String ASSIGNER_REQUETE_BTN_TEXTE = "Assigner la requête à ce membre";
	private static final String AJOUTE_FICHIER_BTN_TEXTE = "Ajouter un fichier";
	private static final String AFFICHER_RAPPORT_BTN_TEXTE = "Afficher le rapport";
	private static final String QUITTER_BTN_TEXTE = "Quitter";
	private static final String PAS_DE_FICHIER_ATTACHE_LABEL = "(pas de fichier attaché)";
	private static final String AJOUTER_COMMENTAIRE_LABEL = "Ajouter commentaire:";
	private static final String COMMENTAIRES_LABEL = "Commentaires:";
	private static final long serialVersionUID = 2101830360063985664L;
	private static final String REQUÊTE_SELECTIONNEE_LABEL = "Requête sélectionnée:";
	private static final String REQUETES_ASSIGNEES_LABEL = "Les requêtes qui vous sont assignées:";
	private static final String AUCUNE_REQUETE_ASSIGNEE_MESSAGE = "Vous n'avez pas encore de requête.";
	private static final String AUCUNE_REQUETE_MESSAGE = "Il n'y a pas de requêtes pour le moment.";
	private static final String AUCUN_TECHNICIEN_MESSAGE = "Il n'y a pas de technicien";
	private static final String[] CATEGORIES = new String[] { "Changer la catégorie", "Poste de travail", "Serveur",
			"Service web", "Compte usager", "Autre" };

	private Utilisateur utilisateur;

	private Requete requeteSelectionnee;
	private File file;
	private boolean isShowComments = false;

	private JFrame prec;

	private DefaultListModel<String> requetesAssigneesListModel = new DefaultListModel<String>();
	private DefaultListModel<String> requetesDisponiblesListModel = new DefaultListModel<String>();
	private DefaultListModel<String> techniciensListModel = new DefaultListModel<String>();

	private JLabel ajoutCommentaireLabel;
	private JLabel commentairesLabel;
	private JLabel requetesAssigneesLabel;
	private JLabel requetesDisponiblesLabel;
	private JLabel requeteSelectionneeLabel;
	private JLabel fichierLabel;

	private JButton afficherRapportBtn;
	private JButton ajoutFichierBtn;
	private JButton assignerRequeteBtn;
	private JButton nouvelleRequeteBtn;
	private JButton prendreRequeteBtn;
	private JButton quitterBtn;

	private JComboBox categoriesBox;
	private JComboBox finBox;

	private JTextField commentaireTextField;

	private JTextArea requeteArea;
	private JTextArea commentsArea;
	private JTextArea descReqArea;

	private JFileChooser fileChooser;

	private JScrollPane jScrollPane1RequetesAssignees;
	private JScrollPane jScrollPaneRequeteArea;
	private JScrollPane jScrollPaneCommentArea;
	private JScrollPane jScrollPaneRequetesDisponibles;
	private JScrollPane jScrollPane5;
	private JScrollPane jScrollPaneSupport;

	private JList<String> supportJList;
	private JList<String> requetesAssigneeJList;
	private JList<String> requetesDisponiblesJList;

	private List<Requete> listRequetesTechnicien;
	private List<Requete> listRequetesDisponibles;
	private List<Utilisateur> listeTechniciens;

	public TechnicienLoggedFrame(Utilisateur technicien) throws FileNotFoundException, IOException, SQLException {
		initComponents();
		this.setVisible(true);
		FrameService.centerFrame(this);

		utilisateur = (Technicien) technicien;

		populerListes(technicien);

		initialiserListesDynamiquesInterfaceTechnicien();
	}

	private void populerListes(Utilisateur technicien) throws FileNotFoundException, IOException, SQLException {
		listRequetesTechnicien = technicien.getListeRequetes();
		listRequetesDisponibles = BanqueRequetes.getInstance().getListRequetes(RequeteStatut.OUVERT);
		listeTechniciens = BanqueUtilisateurs.getUserInstance().getListUtilisateurs(UserRole.TECHNICIEN);

		initialiserListesDynamiquesInterfaceTechnicien();
	}

	private void initialiserListesDynamiquesInterfaceTechnicien() {
		initialiserRequetesAssigneesListModel();
		initialiserRequetesDisponiblesListModel();
		initialiserTechniciensListModel();
	}

	private void initialiserTechniciensListModel() {
		if (listeTechniciens.isEmpty()) {
			techniciensListModel.addElement(AUCUN_TECHNICIEN_MESSAGE);
		} else {
			for (int i = 0; i < listeTechniciens.size(); i++) {
				techniciensListModel.addElement(listeTechniciens.get(i).getNomUtilisateur());
			}
		}

		supportJList.setModel(techniciensListModel);
	}

	private void initialiserRequetesDisponiblesListModel() {
		if (listRequetesDisponibles.isEmpty()) {
			requetesDisponiblesListModel.addElement(AUCUNE_REQUETE_MESSAGE);
			requetesDisponiblesJList.setModel(requetesDisponiblesListModel);
			prendreRequeteBtn.setEnabled(false);
		} else {
			for (int i = 0; i < listRequetesDisponibles.size(); i++) {
				requetesDisponiblesListModel.addElement(listRequetesDisponibles.get(i).getSujet());
			}
			requetesDisponiblesJList.setModel(requetesDisponiblesListModel);
			prendreRequeteBtn.setEnabled(true);
		}
	}

	private void initialiserRequetesAssigneesListModel() {
		if (listRequetesTechnicien.isEmpty()) {
			requetesAssigneesListModel.addElement(AUCUNE_REQUETE_ASSIGNEE_MESSAGE);
			requetesAssigneeJList.setModel(requetesAssigneesListModel);
		} else {
			for (int i = 0; i < listRequetesTechnicien.size(); i++) {
				requetesAssigneesListModel.addElement(listRequetesTechnicien.get(i).getSujet());
			}
			requetesAssigneeJList.setModel(requetesAssigneesListModel);
		}
	}

	private void initComponents() {
		fileChooser = new JFileChooser();
		requetesAssigneesLabel = new JLabel();
		jScrollPane1RequetesAssignees = new JScrollPane();
		requetesAssigneeJList = new JList<String>();
		requeteSelectionneeLabel = new JLabel();
		jScrollPaneRequeteArea = new JScrollPane();
		requeteArea = new JTextArea();
		commentairesLabel = new JLabel();
		jScrollPaneCommentArea = new JScrollPane();
		commentsArea = new JTextArea();
		ajoutCommentaireLabel = new JLabel();
		commentaireTextField = new JTextField();
		categoriesBox = new JComboBox();
		requetesDisponiblesLabel = new JLabel();
		jScrollPaneRequetesDisponibles = new JScrollPane();
		requetesDisponiblesJList = new JList<String>();
		prendreRequeteBtn = new JButton();
		jScrollPane5 = new JScrollPane();
		descReqArea = new JTextArea();
		nouvelleRequeteBtn = new JButton();
		assignerRequeteBtn = new JButton();
		jScrollPaneSupport = new JScrollPane();
		supportJList = new JList<String>();
		finBox = new JComboBox();
		ajoutFichierBtn = new JButton();
		afficherRapportBtn = new JButton();
		quitterBtn = new JButton();
		fichierLabel = new JLabel();

		setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

		setLabels();
		setTexteBoutons();
		setListenersBoutons();

		requetesAssigneeJList.addListSelectionListener(new ListSelectionListener() {
			public void valueChanged(ListSelectionEvent evt) {
				reqAssigneeValueChanged(evt);
			}
		});

		jScrollPane1RequetesAssignees.setViewportView(requetesAssigneeJList);

		requeteArea.setColumns(20);
		requeteArea.setRows(5);

		requeteArea.addPropertyChangeListener(new PropertyChangeListener() {
			public void propertyChange(PropertyChangeEvent evt) {
				requeteAreaPropertyChange(evt);
			}
		});

		jScrollPaneRequeteArea.setViewportView(requeteArea);

		commentsArea.setColumns(20);
		commentsArea.setRows(5);
		jScrollPaneCommentArea.setViewportView(commentsArea);

		commentaireTextField.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				comInActionPerformed(evt);
			}
		});
		commentaireTextField.addKeyListener(new KeyAdapter() {
			public void keyPressed(KeyEvent evt) {
				try {
					ajouterCommentaireOnKeyPressed(evt);
				} catch (IOException e) {
					e.printStackTrace();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		});

		categoriesBox.setModel(new DefaultComboBoxModel(CATEGORIES));
		categoriesBox.addItemListener(new java.awt.event.ItemListener() {
			public void itemStateChanged(java.awt.event.ItemEvent evt) {
				catBoxItemStateChanged(evt);
			}
		});

		categoriesBox.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				categorieBoxActionPerformed(evt);
			}
		});

		requetesDisponiblesJList.addListSelectionListener(new ListSelectionListener() {
			public void valueChanged(ListSelectionEvent evt) {
				reqDispoValueChanged(evt);
			}
		});
		jScrollPaneRequetesDisponibles.setViewportView(requetesDisponiblesJList);

		prendreRequeteBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				try {
					prendreReqBtnActionPerformee(evt);
				} catch (IOException e) {
					e.printStackTrace();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		});

		descReqArea.setColumns(20);
		descReqArea.setRows(5);
		jScrollPane5.setViewportView(descReqArea);

		jScrollPaneSupport.setViewportView(supportJList);

		finBox.setModel(new javax.swing.DefaultComboBoxModel(CHOIX_ACTIONS_REQUETES));
		finBox.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent evt) {
				finBoxItemStateChanged(evt);
			}
		});

		finBox.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				finBoxActionPerformed(evt);
			}
		});

		fichierLabel.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent evt) {
				fileLblMouseClicked(evt);
			}
		});

		GroupLayout layout = new GroupLayout(getContentPane());
		getContentPane().setLayout(layout);
		setHorizontalGroup(layout);
		setVerticalGroup(layout);

		pack();
	}

	private void setListenersBoutons() {
		ajoutFichierBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				ajoutfichierBtnActionPerformed(evt);
			}
		});

		afficherRapportBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				affRapportBtnActionPerformed(evt);
			}
		});

		quitterBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				quitBtnActionPerformed(evt);
			}
		});

		nouvelleRequeteBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				try {
					newRequeteBtnActionPerformed(evt);
				} catch (IOException e) {
					e.printStackTrace();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		});

		assignerRequeteBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				try {
					assignerReqBtnActionPerformed(evt);
				} catch (IOException e) {
					e.printStackTrace();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		});
	}

	private void setTexteBoutons() {
		assignerRequeteBtn.setText(ASSIGNER_REQUETE_BTN_TEXTE);
		ajoutFichierBtn.setText(AJOUTE_FICHIER_BTN_TEXTE);
		afficherRapportBtn.setText(AFFICHER_RAPPORT_BTN_TEXTE);
		quitterBtn.setText(QUITTER_BTN_TEXTE);
		prendreRequeteBtn.setText(PRENDRE_REQUETE_BTN_TEXTE);
		nouvelleRequeteBtn.setText(NOUVELLE_REQUÊTE_BTN_TEXTE);
	}

	private void setLabels() {
		requetesAssigneesLabel.setText(REQUETES_ASSIGNEES_LABEL);
		requeteSelectionneeLabel.setText(REQUÊTE_SELECTIONNEE_LABEL);
		commentairesLabel.setText(COMMENTAIRES_LABEL);
		ajoutCommentaireLabel.setText(AJOUTER_COMMENTAIRE_LABEL);
		fichierLabel.setText(PAS_DE_FICHIER_ATTACHE_LABEL);
		requetesDisponiblesLabel.setText(REQUETES_DISPONIBLES_LABEL);
	}

	private void setVerticalGroup(javax.swing.GroupLayout layout) {
		layout.setVerticalGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addGroup(layout
				.createSequentialGroup()
				.addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
						.addGroup(layout.createSequentialGroup().addGap(51, 51, 51).addComponent(nouvelleRequeteBtn)
								.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
								.addComponent(afficherRapportBtn)
								.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
								.addComponent(quitterBtn))
						.addGroup(layout.createSequentialGroup().addContainerGap().addComponent(requetesAssigneesLabel)
								.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
								.addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
										.addComponent(jScrollPane1RequetesAssignees,
												javax.swing.GroupLayout.DEFAULT_SIZE, 171, Short.MAX_VALUE)
										.addGroup(layout
												.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
												.addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout
														.createSequentialGroup().addComponent(commentairesLabel)
														.addPreferredGap(
																javax.swing.LayoutStyle.ComponentPlacement.RELATED)
														.addComponent(jScrollPaneCommentArea,
																javax.swing.GroupLayout.PREFERRED_SIZE,
																javax.swing.GroupLayout.DEFAULT_SIZE,
																javax.swing.GroupLayout.PREFERRED_SIZE)
														.addPreferredGap(
																javax.swing.LayoutStyle.ComponentPlacement.RELATED)
														.addGroup(layout
																.createParallelGroup(
																		javax.swing.GroupLayout.Alignment.BASELINE)
																.addComponent(ajoutCommentaireLabel)
																.addComponent(commentaireTextField,
																		javax.swing.GroupLayout.PREFERRED_SIZE,
																		javax.swing.GroupLayout.DEFAULT_SIZE,
																		javax.swing.GroupLayout.PREFERRED_SIZE))
														.addPreferredGap(
																javax.swing.LayoutStyle.ComponentPlacement.RELATED,
																javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
														.addGroup(layout
																.createParallelGroup(
																		javax.swing.GroupLayout.Alignment.BASELINE)
																.addComponent(ajoutFichierBtn)
																.addComponent(fichierLabel)))
												.addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout
														.createSequentialGroup().addComponent(requeteSelectionneeLabel)
														.addPreferredGap(
																javax.swing.LayoutStyle.ComponentPlacement.RELATED)
														.addComponent(jScrollPaneRequeteArea,
																javax.swing.GroupLayout.PREFERRED_SIZE,
																javax.swing.GroupLayout.DEFAULT_SIZE,
																javax.swing.GroupLayout.PREFERRED_SIZE)
														.addPreferredGap(
																javax.swing.LayoutStyle.ComponentPlacement.RELATED)
														.addComponent(categoriesBox,
																javax.swing.GroupLayout.PREFERRED_SIZE,
																javax.swing.GroupLayout.DEFAULT_SIZE,
																javax.swing.GroupLayout.PREFERRED_SIZE)
														.addPreferredGap(
																javax.swing.LayoutStyle.ComponentPlacement.RELATED)
														.addComponent(finBox, javax.swing.GroupLayout.PREFERRED_SIZE,
																javax.swing.GroupLayout.DEFAULT_SIZE,
																javax.swing.GroupLayout.PREFERRED_SIZE))))
								.addGap(3, 3, 3)))
				.addGap(38, 38, 38).addComponent(requetesDisponiblesLabel)
				.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
				.addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
						.addComponent(jScrollPaneRequetesDisponibles, javax.swing.GroupLayout.PREFERRED_SIZE, 154,
								javax.swing.GroupLayout.PREFERRED_SIZE)
						.addGroup(layout.createSequentialGroup().addComponent(prendreRequeteBtn)
								.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
								.addComponent(jScrollPane5, javax.swing.GroupLayout.DEFAULT_SIZE, 121, Short.MAX_VALUE))
						.addGroup(layout.createSequentialGroup().addComponent(assignerRequeteBtn)
								.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
								.addComponent(jScrollPaneSupport, javax.swing.GroupLayout.DEFAULT_SIZE, 121,
										Short.MAX_VALUE)))
				.addContainerGap()));
	}

	private void setHorizontalGroup(javax.swing.GroupLayout layout) {
		layout.setHorizontalGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addGroup(
				javax.swing.GroupLayout.Alignment.TRAILING,
				layout.createSequentialGroup().addContainerGap().addGroup(layout
						.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
						.addGroup(layout.createSequentialGroup().addGroup(layout
								.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
								.addComponent(requetesDisponiblesLabel)
								.addGroup(layout.createSequentialGroup()
										.addComponent(jScrollPaneRequetesDisponibles,
												javax.swing.GroupLayout.PREFERRED_SIZE, 223,
												javax.swing.GroupLayout.PREFERRED_SIZE)
										.addGap(32, 32, 32)
										.addGroup(layout
												.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
												.addComponent(jScrollPane5).addComponent(prendreRequeteBtn))
										.addGap(18, 18, 18)
										.addGroup(layout
												.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
												.addComponent(jScrollPaneSupport,
														javax.swing.GroupLayout.Alignment.LEADING, 0, 0,
														Short.MAX_VALUE)
												.addComponent(assignerRequeteBtn,
														javax.swing.GroupLayout.Alignment.LEADING))))
								.addGap(86, 86, 86))
						.addGroup(layout.createSequentialGroup().addGroup(layout
								.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
								.addComponent(jScrollPane1RequetesAssignees, javax.swing.GroupLayout.DEFAULT_SIZE, 208,
										Short.MAX_VALUE)
								.addComponent(requetesAssigneesLabel, javax.swing.GroupLayout.Alignment.TRAILING,
										javax.swing.GroupLayout.DEFAULT_SIZE, 208, Short.MAX_VALUE))
								.addGap(18, 18, 18)
								.addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
										.addComponent(requeteSelectionneeLabel)
										.addComponent(jScrollPaneRequeteArea, javax.swing.GroupLayout.PREFERRED_SIZE,
												javax.swing.GroupLayout.DEFAULT_SIZE,
												javax.swing.GroupLayout.PREFERRED_SIZE)
										.addComponent(categoriesBox, javax.swing.GroupLayout.PREFERRED_SIZE, 142,
												javax.swing.GroupLayout.PREFERRED_SIZE)
										.addComponent(finBox, javax.swing.GroupLayout.PREFERRED_SIZE,
												javax.swing.GroupLayout.DEFAULT_SIZE,
												javax.swing.GroupLayout.PREFERRED_SIZE))
								.addGap(21, 21, 21)
								.addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
										.addGroup(layout
												.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
												.addComponent(jScrollPaneCommentArea,
														javax.swing.GroupLayout.PREFERRED_SIZE, 289,
														javax.swing.GroupLayout.PREFERRED_SIZE)
												.addComponent(commentairesLabel)
												.addGroup(layout.createSequentialGroup()
														.addComponent(ajoutCommentaireLabel)
														.addPreferredGap(
																javax.swing.LayoutStyle.ComponentPlacement.RELATED)
														.addComponent(commentaireTextField)))
										.addGroup(layout.createSequentialGroup().addComponent(ajoutFichierBtn)
												.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
												.addComponent(fichierLabel)))
								.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 31,
										Short.MAX_VALUE)))
						.addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
								.addComponent(quitterBtn, javax.swing.GroupLayout.DEFAULT_SIZE,
										javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
								.addComponent(afficherRapportBtn, javax.swing.GroupLayout.DEFAULT_SIZE,
										javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
								.addComponent(nouvelleRequeteBtn, javax.swing.GroupLayout.DEFAULT_SIZE,
										javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
						.addContainerGap()));
	}

	private void quitBtnActionPerformed(ActionEvent evt) {
		System.exit(0);
	}

	private void requeteAreaPropertyChange(PropertyChangeEvent evt) {
		// TODO add your handling code here:
	}

	// On change la sortie du TextArea en fonction de la requete selectionnée
	private void reqAssigneeValueChanged(ListSelectionEvent evt) {
		updateRequeteAssignees();
		updateCommentaires();
	}

	// On change la sortie du TextArea en fonction de la requete selectionnée
	private void reqDispoValueChanged(ListSelectionEvent evt) {
		if (!listRequetesDisponibles.isEmpty()) {
			updateRequetesDisponibles();
		}
	}

	private void catBoxItemStateChanged(ItemEvent evt) {
		changerCategorie();
	}

	private void changerCategorie() {
		listRequetesTechnicien.get(requetesAssigneeJList.getSelectedIndex())
				.setCategorie(RequeteCategorie.fromString((String) categoriesBox.getSelectedItem()));
		updateRequeteAssignees();
	}

	private void prendreReqBtnActionPerformee(ActionEvent evt) throws IOException, SQLException {
		Requete requete = listRequetesDisponibles.get(requetesDisponiblesJList.getSelectedIndex());
		requete.setTechnicien((Technicien) utilisateur);
		requete.setClient((Technicien) utilisateur);
		requete.setStatut(RequeteStatut.EN_TRAITEMENT);

		listRequetesTechnicien.add(requete);

		requetesAssigneesListModel = new DefaultListModel<String>();
		for (int i = 0; i < listRequetesTechnicien.size(); i++) {
			requetesAssigneesListModel.addElement(listRequetesTechnicien.get(i).getSujet());
		}
		requetesAssigneeJList.setModel(requetesAssigneesListModel);

		requetesDisponiblesListModel.remove(requetesDisponiblesJList.getSelectedIndex());
		listRequetesDisponibles.remove(requete);

		BanqueRequetes.getInstance().assignerRequete((Technicien) utilisateur, requete);
	}

	private void comInActionPerformed(ActionEvent evt) {
		// TODO add your handling code here:.
	}

	// Ajoute le commentaire lorsqu'on appuie sur ENTER
	private void ajouterCommentaireOnKeyPressed(KeyEvent evt) throws IOException, SQLException {
		if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
			requeteSelectionnee.addCommentaire(commentaireTextField.getText(), utilisateur);
			BanqueRequetes.getInstance().assignerRequete((Technicien) utilisateur, requeteSelectionnee);
			commentsArea.setText(getListeCommentairesEnString());
			commentaireTextField.setText("");
		}
	}

	private void ajoutfichierBtnActionPerformed(ActionEvent evt) {// GEN-FIRST:event_ajoutfichierBtnActionPerformed
		fileChooser.showOpenDialog(prec);

		File f = fileChooser.getSelectedFile();
		if (f != null) {
			try {
				file = new File("dat/" + f.getName());
				InputStream srcFile = new FileInputStream(f);
				OutputStream newFile = new FileOutputStream(file);
				byte[] buf = new byte[4096];
				int len;
				while ((len = srcFile.read(buf)) > 0) {
					newFile.write(buf, 0, len);
				}
				srcFile.close();
				newFile.close();
			} catch (IOException ex) {
				Logger.getLogger(NouvelleRequeteFrame.class.getName()).log(Level.SEVERE, null, ex);
			}
			if (file.exists()) {
				FichierAddOkFrame ok = new FichierAddOkFrame();
				ok.setVisible();
			}
		}

		try {
			listRequetesTechnicien.get(requetesAssigneeJList.getSelectedIndex()).setFichier(file);
		} catch (IndexOutOfBoundsException e) {
		}

		updateRequeteAssignees();
	}

	private void fileLblMouseClicked(java.awt.event.MouseEvent evt) {
		openFile();
	}

	private void openFile() {
		try {
			Requete req = listRequetesTechnicien.get(requetesAssigneeJList.getSelectedIndex());
			java.awt.Desktop dt = java.awt.Desktop.getDesktop();
			dt.open(req.getFichier());
		} catch (IOException ex) {
			Logger.getLogger(TechnicienLoggedFrame.class.getName()).log(Level.SEVERE, null, ex);
		}
	}

	private void finBoxActionPerformed(java.awt.event.ActionEvent evt) {
		// TODO add your handling code here:
	}

	private void categorieBoxActionPerformed(java.awt.event.ActionEvent evt) {
		// TODO add your handling code here:
	}

	private void finBoxItemStateChanged(java.awt.event.ItemEvent evt) {
		finaliserRequete();
	}

	private void finaliserRequete() {
		if (finBox.getSelectedIndex() > 0) {
			listRequetesTechnicien.get(requetesAssigneeJList.getSelectedIndex())
					.setStatut(RequeteStatut.fromString((String) finBox.getSelectedItem()));
			updateRequeteAssignees();
		}
	}

	// Assigne une requete à un autre membre selectionné dans la liste
	private void assignerReqBtnActionPerformed(ActionEvent evt) throws IOException, SQLException {
		Requete requete = listRequetesDisponibles.get(requetesDisponiblesJList.getSelectedIndex());
		Technicien technicien = (Technicien) listeTechniciens.get(supportJList.getSelectedIndex());

		requetesDisponiblesListModel.remove(requetesDisponiblesJList.getSelectedIndex());
		listRequetesDisponibles.remove(requete);

		BanqueRequetes.getInstance().assignerRequete(technicien, requete);

		updateTables();
		System.out.println(technicien.getListPerso().size());
		System.out.println(technicien.getListeRequetes().size());
	}

	private void newRequeteBtnActionPerformed(java.awt.event.ActionEvent evt) throws IOException, SQLException {
		this.setVisible(false);
		new NouvelleRequeteFrame(utilisateur, this);

		listRequetesDisponibles = BanqueRequetes.getInstance().getListRequetes(RequeteStatut.OUVERT);
	}

	private void affRapportBtnActionPerformed(java.awt.event.ActionEvent evt) {
		String rapportContenu = ServiceTechnicien.genererRepport(listeTechniciens);
		RapportFrame rapport = new RapportFrame(rapportContenu);
		rapport.setVisible(true);
	}

	private void updateRequeteAssignees() {
		setContenuRequeteArea();

		if (requeteSelectionnee.getStatut() == RequeteStatut.EN_TRAITEMENT) {
			finBox.setEnabled(true);
		} else {
			finBox.setEnabled(false);
		}
	}

	private void setContenuRequeteArea() {

		requeteSelectionnee = listRequetesTechnicien.get(requetesAssigneeJList.getSelectedIndex());
		requeteArea.setText("Sujet: " + requeteSelectionnee.getSujet() + "\nDescription: "
				+ requeteSelectionnee.getDescription() + "\nCatégorie: " + requeteSelectionnee.getCategorie().toString()
				+ "\nStatut: " + requeteSelectionnee.getStatut().toString());

		commentsArea.setText(getListeCommentairesEnString());

		if (isFichierExiste()) {
			fichierLabel.setVisible(true);
			fichierLabel.setText("Afficher " + requeteSelectionnee.getFichier().getPath());
		} else {
			fichierLabel.setVisible(false);
		}

	}

	private boolean isFichierExiste() {
		return requeteSelectionnee.getFichier() != null && requeteSelectionnee.getFichier().exists();
	}

	private void updateRequetesDisponibles() {
		if (requetesDisponiblesJList.getSelectedIndex() >= 0) {
			requeteSelectionnee = listRequetesDisponibles.get(requetesDisponiblesJList.getSelectedIndex());
			descReqArea.setText("Sujet: " + requeteSelectionnee.getSujet() + "\nDescription: "
					+ requeteSelectionnee.getDescription() + "\nCatégorie: "
					+ requeteSelectionnee.getCategorie().toString() + "\nStatut: "
					+ requeteSelectionnee.getStatut().toString());
		} else {
			descReqArea.setText("");
		}
	}

	private void updateCommentaires() {
		if (isRequeteSelectionneeEtCommentairesAffiches()) {
			commentsArea.setText(getListeCommentairesEnString());
			isShowComments = true;
		}
	}

	private boolean isRequeteSelectionneeEtCommentairesAffiches() {
		return requeteSelectionnee != null && !isShowComments;
	}

	private String getListeCommentairesEnString() {
		String s = "";

		for (Commentaire c : requeteSelectionnee.getListeCommentaires()) {
			s += c.toString();
		}

		return s;
	}

	private void updateTables() {
		requetesAssigneeJList.setModel(requetesAssigneesListModel);
		if (!requetesDisponiblesListModel.isEmpty()) {
			requetesDisponiblesJList.setModel(requetesDisponiblesListModel);
			prendreRequeteBtn.setEnabled(true);
		} else {
			requetesDisponiblesListModel.addElement(AUCUNE_REQUETE_MESSAGE);
			requetesDisponiblesJList.setModel(requetesDisponiblesListModel);
			prendreRequeteBtn.setEnabled(false);
		}
	}
}
