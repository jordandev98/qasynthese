package com.cranberrysupport.gui;

public class RapportFrame extends Frame {

	private static final long serialVersionUID = 3288503971746231879L;
	
	private javax.swing.JButton closeFrameButton;
	private javax.swing.JScrollPane jScrollPane1;
	private javax.swing.JTextArea rapportArea;

	public RapportFrame(String t) {
		initComponents();
		rapportArea.setText(t);
		setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
	}

	private void initComponents() {

		jScrollPane1 = new javax.swing.JScrollPane();
		rapportArea = new javax.swing.JTextArea();

		setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

		rapportArea.setColumns(20);
		rapportArea.setEditable(false);
		rapportArea.setLineWrap(true);
		rapportArea.setRows(5);
		jScrollPane1.setViewportView(rapportArea);

		closeFrameButton = new javax.swing.JButton();
		closeFrameButton.setText("Fermer");
		closeFrameButton.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				jButton1ActionPerformed(evt);
			}
		});

		javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
		getContentPane().setLayout(layout);
		layout.setHorizontalGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addGroup(layout
				.createSequentialGroup()
				.addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
						.addGroup(layout.createSequentialGroup().addGap(18, 18, 18).addComponent(jScrollPane1,
								javax.swing.GroupLayout.PREFERRED_SIZE, 214, javax.swing.GroupLayout.PREFERRED_SIZE))
						.addGroup(layout.createSequentialGroup().addGap(89, 89, 89).addComponent(closeFrameButton)))
				.addContainerGap(22, Short.MAX_VALUE)));
		layout.setVerticalGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
				.addGroup(layout.createSequentialGroup().addContainerGap()
						.addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 457,
								javax.swing.GroupLayout.PREFERRED_SIZE)
						.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
						.addComponent(closeFrameButton).addContainerGap(18, Short.MAX_VALUE)));

		pack();
	}

	private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {
		this.setVisible(false);
	}
}
