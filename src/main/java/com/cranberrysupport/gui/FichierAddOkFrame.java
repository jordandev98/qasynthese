package com.cranberrysupport.gui;

import javax.swing.JButton;
import javax.swing.JLabel;

import com.cranberrysupport.service.FrameService;

public class FichierAddOkFrame extends Frame {

	private static final long serialVersionUID = -7407705667229077936L;

	private static final String OK_BTN_TEXTE = "OK";
	private static final String SUCCES_MESSAGE_LABEL = "Fichier bien ajouté à la requête";

	private JButton okBtn;
	private JLabel jLabel;

	public FichierAddOkFrame() {
		initComponents();
	}

	private void initComponents() {

		jLabel = new JLabel();
		okBtn = new JButton();

		setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

		jLabel.setText(SUCCES_MESSAGE_LABEL);

		okBtn.setText(OK_BTN_TEXTE);
		okBtn.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				jButton1ActionPerformed(evt);
			}
		});

		javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
		getContentPane().setLayout(layout);
		layout.setHorizontalGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addGroup(
				javax.swing.GroupLayout.Alignment.TRAILING,
				layout.createSequentialGroup().addContainerGap(71, Short.MAX_VALUE)
						.addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.CENTER)
								.addComponent(okBtn).addComponent(jLabel))
						.addGap(67, 67, 67)));
		layout.setVerticalGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
				.addGroup(layout.createSequentialGroup().addGap(22, 22, 22).addComponent(jLabel)
						.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED).addComponent(okBtn)
						.addContainerGap(23, Short.MAX_VALUE)));

		pack();
	}

	private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {
		this.setVisible(false);
	}

	public void setVisible() {
		this.setVisible(true);
		FrameService.centerFrame(this);
	}

}
