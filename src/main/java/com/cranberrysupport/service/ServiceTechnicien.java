package com.cranberrysupport.service;

import java.util.List;

import com.cranberrysupport.model.Technicien;
import com.cranberrysupport.model.Utilisateur;

public class ServiceTechnicien {

	public static String genererRepport(List<Utilisateur> techniciens) {
		String rap = "";
		Utilisateur tempo;
		for (int i = 0; i < techniciens.size(); i++) {
			rap += techniciens.get(i).getNomUtilisateur() + ":\n"; 
			tempo = techniciens.get(i);
			rap += ((Technicien) tempo).getRequeteParStatut() + "\n";

		}
		return rap;
	}

}
