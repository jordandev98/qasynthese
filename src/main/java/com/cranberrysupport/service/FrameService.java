package com.cranberrysupport.service;

import java.awt.Dimension;
import java.awt.Toolkit;

import com.cranberrysupport.gui.Frame;

public class FrameService {

	public static void centerFrame(Frame frame) {
		Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
        frame.setLocation(dim.width/2-frame.getSize().width/2, dim.height/2-frame.getSize().height/2);
	}

}
