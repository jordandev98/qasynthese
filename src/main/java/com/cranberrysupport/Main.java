package com.cranberrysupport;

/**
 * @author Jordan Lee et Amina Mihoubi
 */

import com.cranberrysupport.controller.RequeteControleur;
import com.cranberrysupport.gui.CranberryFrame;
import com.cranberrysupport.model.*;
import com.cranberrysupport.util.JdbcUtil;
import com.cranberrysupport.util.RequeteDao;
import com.cranberrysupport.util.UtilisateurDao;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.UIManager;
import javax.swing.UIManager.LookAndFeelInfo;

public class Main {
	public static void main(String args[]) throws FileNotFoundException, IOException, SQLException {
		// TODO EXECUTER QUE A LA PREMIERE EXECUTION , POUR POUVOIR IMPORTER LA BD PAR
		// DEFAULT. Peut être supprimé une fois la BD importée
		// dropTables();
		// initBD();

		RequeteControleur requeteControleur = new RequeteControleur();

		try {
			for (LookAndFeelInfo info : UIManager.getInstalledLookAndFeels()) {
				if ("Nimbus".equals(info.getName())) {
					javax.swing.UIManager.setLookAndFeel(info.getClassName());
					break;
				}
			}
		} catch (ClassNotFoundException ex) {
			java.util.logging.Logger.getLogger(CranberryFrame.class.getName()).log(java.util.logging.Level.SEVERE, null,
					ex);
		} catch (InstantiationException ex) {
			java.util.logging.Logger.getLogger(CranberryFrame.class.getName()).log(java.util.logging.Level.SEVERE, null,
					ex);
		} catch (IllegalAccessException ex) {
			java.util.logging.Logger.getLogger(CranberryFrame.class.getName()).log(java.util.logging.Level.SEVERE, null,
					ex);
		} catch (javax.swing.UnsupportedLookAndFeelException ex) {
			java.util.logging.Logger.getLogger(CranberryFrame.class.getName()).log(java.util.logging.Level.SEVERE, null,
					ex);
		}

		displayForm();

		Runtime.getRuntime().addShutdownHook(new Thread(new Runnable() {
			@Override
			public void run() {
				try {
					requeteControleur.saveRequetes();
				} catch (IOException | SQLException ex) {
					Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
				}
			}
		}));
	}

	private static void dropTables() throws SQLException {
		Connection connection = JdbcUtil.getConnection();
		Statement statement = connection.createStatement();
		String sqlDrop = "DROP TABLE Requete, Utilisateur";
		statement.executeUpdate(sqlDrop);
		connection.close();
	}

	private static void initBD() throws SQLException, IOException {
		UtilisateurDao utilisateurDao = new UtilisateurDao();
		utilisateurDao.creerTableUtilisateur();
		RequeteDao requeteDao = new RequeteDao();
		requeteDao.creerTableRequete();

		Client roger = new Client("Roger", "Danfousse", "rogerdanfousse", "jesuiscool");
		Client martine = new Client("Martine", "Jodoin", "martinejodoin", "moiaussi");
		Client popoline = new Client("Pauline", "St-Onge", "popoline", "monchatkiki");
		Technicien richard = new Technicien("Richard", "Chabot", "chabotr", "chabotte");
		Technicien louise = new Technicien("Louise", "Boisvert", "techguest", "password");
		utilisateurDao.addUtilisateur(roger);
		utilisateurDao.addUtilisateur(martine);
		utilisateurDao.addUtilisateur(popoline);
		utilisateurDao.addUtilisateur(richard);
		utilisateurDao.addUtilisateur(louise);

		Requete requete1 = new Requete("Exemple 1", "test une nouvelle requete", louise,
				RequeteCategorie.posteDeTravail);
		requete1.setStatut(RequeteStatut.SUCCES);

		Requete requete2 = new Requete("Exemple 2", "Une autre requete", louise, RequeteCategorie.posteDeTravail);
		requete2.setStatut(RequeteStatut.SUCCES);
		Requete requete3 = new Requete("TEST", "TEST", louise, RequeteCategorie.serveur);
		requete3.setStatut(RequeteStatut.OUVERT);
		Requete requete4 = new Requete("exemple1", "test d une nouvelle requete", richard,
				RequeteCategorie.posteDeTravail);
		requete4.setStatut(RequeteStatut.OUVERT);

		requeteDao.sauvegardeRequete(requete1);
		requeteDao.sauvegardeRequete(requete2);
		requeteDao.sauvegardeRequete(requete3);
		requeteDao.sauvegardeRequete(requete4);
	}

	private static void displayForm() {
		java.awt.EventQueue.invokeLater(new Runnable() {
			@Override
			public void run() {
				new CranberryFrame().setVisible(true);
			}
		});
	}
}
