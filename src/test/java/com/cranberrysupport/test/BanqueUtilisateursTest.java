package com.cranberrysupport.test;

import com.cranberrysupport.model.BanqueUtilisateurs;
import com.cranberrysupport.model.UserRole;
import com.cranberrysupport.model.Utilisateur;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.IOException;
import java.sql.SQLException;

import static org.junit.Assert.*;

public class BanqueUtilisateursTest{
	private static BanqueUtilisateurs banqueUtilisateurs;


	@BeforeClass
	public static void setUp() throws IOException, SQLException {
		banqueUtilisateurs = BanqueUtilisateurs.getUserInstance();
	}
	
	@Test
	public void testLectureUtilisateurs() {
		assertEquals(banqueUtilisateurs.getListUtilisateurs().size(), 5);
	}
	
	@Test
	public void testLectureClients() {
		assertEquals(banqueUtilisateurs.getListUtilisateurs(UserRole.CLIENT).size(), 3);
	}
	
	@Test
	public void testLectureTechniciens() {
		assertEquals(banqueUtilisateurs.getListUtilisateurs(UserRole.TECHNICIEN).size(), 2);
	}
	
	@Test
	public void testChercherUtilisateurExistant() {
		Utilisateur utilisateur = banqueUtilisateurs.chercherUtilisateurByNomUtilisateur("techguest");
		assertNotNull(utilisateur);
		assertEquals(utilisateur.getUserRole(), UserRole.TECHNICIEN);
		assertEquals("Boisvert", utilisateur.getNom());
		assertEquals("Louise", utilisateur.getPrenom());
	}

	@Test
	public void testChercherUtilisateurInexistant() {
		assertNull(banqueUtilisateurs.chercherUtilisateurByNomUtilisateur("invalide"));

	}
	
//	@Test
//	public void testLoginTechnicienWithGoodPassword() {
//		Utilisateur utilisateur = banqueUtilisateurs.chercherUtilisateurByNomUtilisateur("techguest");
//		utilisateur.
//	}
}