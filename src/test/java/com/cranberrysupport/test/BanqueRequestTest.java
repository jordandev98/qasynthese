package com.cranberrysupport.test;

import com.cranberrysupport.model.BanqueRequetes;
import com.cranberrysupport.model.*;
import com.cranberrysupport.util.RequeteDao;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class BanqueRequestTest {
    BanqueRequetes banqueRequetes;

    @Before
    public void setUp() throws IOException, SQLException {
        banqueRequetes = BanqueRequetes.getInstance();
    }



    @Test
    public void newRequeteTest() throws IOException, SQLException {

        Utilisateur client = new Client("nomTest", "prenomTest", "utilisateurTest", "motDePasse");
        banqueRequetes.addNouvelleRequete(new Requete("test", "description", client, RequeteCategorie.posteDeTravail));
        Assert.assertEquals(1, client.getListeRequetes().size());
    }

    @Test
    public void newRequeteManyTest() throws IOException, SQLException {
        int nombreDeRequeteAttendu = 3;
        Utilisateur client = new Client("nomTest", "prenomTest", "utilisateurTest", "motDePasse");

        for (int i = 0; i < 3; i++) {
            banqueRequetes.addNouvelleRequete(
                    new Requete("test" + i, "description" + i, client, RequeteCategorie.posteDeTravail));
        }
        Assert.assertEquals(nombreDeRequeteAttendu, client.getListeRequetes().size());
    }

    @Test
    public void returnLastTest() throws IOException, SQLException {
        Utilisateur client = new Client("nomTest", "prenomTest", "utilisateurTest", "motDePasse");
        banqueRequetes.addNouvelleRequete(new Requete("test", "description", client, RequeteCategorie.posteDeTravail));
        banqueRequetes
                .addNouvelleRequete(new Requete("test1", "description1", client, RequeteCategorie.posteDeTravail));
        Assert.assertEquals(banqueRequetes.getDerniereRequete().getSujet(), "test1");
        Assert.assertEquals(banqueRequetes.getDerniereRequete().getDescription(), "description1");
    }

    @Test
    public void getListRequetes() throws IOException, SQLException {
        Utilisateur client = new Client("nomTest", "prenomTest", "utilisateurTest", "motDePasse");
        banqueRequetes.addNouvelleRequete(new Requete("test", "description", client, RequeteCategorie.posteDeTravail));
        Utilisateur client2 = new Client("nomTest", "prenomTest", "utilisateurTest", "motDePasse");
        banqueRequetes.addNouvelleRequete(new Requete("test2", "description2", client2, RequeteCategorie.serveur));
        List<Requete> requetes = banqueRequetes.getListRequetes(RequeteStatut.OUVERT);
        Assert.assertTrue(requetes.size() >= 3);
    }

    private List<Requete> getMockListRequete( ) throws IOException {
        Utilisateur client = new Client("nomTest", "prenomTest", "utilisateurTest", "motDePasse");
        List<Requete> requetes = new ArrayList<>();
        Requete requete =new Requete("sujetTest" , "descriptionTest" , client , RequeteCategorie.posteDeTravail);
        requete.setStatut(RequeteStatut.ABANDON);
        Requete requete1 =new Requete("sujetTest2" , "descriptionTest2" , client , RequeteCategorie.posteDeTravail);
        requete1.setStatut(RequeteStatut.ABANDON);
        requetes.add(requete);
        requetes.add(requete1);

        return requetes;
    }

}
